# CNWN

A small C99 library for Neverwinter Nights (Aurora) stuff.

## License

MIT, see LICENSE

Use at your own peril! The author is not responsible.

## Tools

So far only the cnwn-erf tool is in progress. It seems to work, but backup your modules just in case.

Build like this (out of source):
```
cd <project dir>
mkdir build
cd build
cmake ..
make
make install
```

Haven't made proper install target yet, but the binaries should be under '<project dir>/build'.

### cnwn-erf

```
cnwn-erf [options] <command> <ERF file> [args]
0.1.0
  General options:
    -h, --help	Show this help and exit.
    -V, --show-version	Show version and exit.

  list [options] <ERF file[regular expressions]
    -i, --info	Show ERF info at top.
    -l, --localized-strings	List localized strings.
    -S, --summary	Show summary at end.
    -s, --size	Show entry sizes.
    -O, --offset	Show entry offsets.
    -n, --no-entries	Don't list entries.

  create [options] <ERF file[files]
    -v <VERSION>, --set-version=<VERSION>	Set version.
    -y <YEAR>, --set-year=<YEAR>	Set year.
    -d <DOY>, --set-day-of-year=<DOY>	Set day of year.
    -q, --quiet	Supress output to stdout.

  extract [options] <ERF file[regular expressions]
    -M, --no-meta	Do not extract erf-meta file.
    -S, --summary	Show summary at end.
    -s, --size	Show entry sizes.
    -o <DIR>, --output=<DIR>	Extract to this directory.
    -q, --quiet	Supress output to stdout.
```

## API

- API for creating and extracting ERF (HAK, MOD, NWM) files.
- API for creating and extracting KEY/BIFF files.



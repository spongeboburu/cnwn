#include "cnwn/resource/erf.h"


void try_info(const char * path)
{
    cnwn_File * input_file = cnwn_file_open(path, "r");
    if (input_file != NULL) {
        cnwn_ERFHeader header = {0};
        int64_t ret = cnwn_erf_header_read(&header, input_file);
        if (ret >= 0) {
            printf("Size: %"PRId64"\n", cnwn_file_size(input_file));
            printf("Type: %s\n", header.type);
            printf("Version: %s\n", header.version);
            printf("Localized strings count: %d\n", header.num_localized_strings);
            printf("Localized strings offset: 0x%x\n", header.localized_strings_offset);
            printf("Localized strings size: %d\n", header.localized_strings_size);
            printf("Entry count: %d\n", header.num_entries);
            printf("Keys offset: 0x%x\n", header.keys_offset);
            printf("Values offset: 0x%x\n", header.values_offset);
            printf("Description sttref: %u\n", header.description_strref);
            printf("Year: %u\n", header.year);
            printf("Day of year: %u\n", header.day_of_year);
        } else
            fprintf(stderr, "ERROR: %s\n", cnwn_get_error());
        cnwn_file_close(input_file);
    } else
        fprintf(stderr, "ERROR: %s\n", cnwn_get_error());
}

// void try_extract(const char * path, const char * output_dir)
// {
//     char clean_output_dir[CNWN_PATH_MAX_SIZE];
//     cnwn_path_clean(clean_output_dir, sizeof(clean_output_dir), output_dir);
//     cnwn_File * input_file = cnwn_file_open(path, "r");
//     if (input_file == NULL) {
//         fprintf(stderr, "ERROR: %s\n", cnwn_get_error());
//         return;
//     }
//     cnwn_ERF erf;
//     if (cnwn_erf_init_from_file(&erf, input_file) >= 0) {
//         char path[CNWN_PATH_MAX_SIZE];
//         cnwn_path_cat(path, sizeof(path), clean_output_dir, "erf-info", NULL);
//         int64_t ret = cnwn_erf_write_to_info_path(&erf, path);
//         if (ret >= 0)
//             printf("'erf-info' => '%s' %"PRId64"\n", path, ret);
//         else
//             fprintf(stderr, "ERROR: %s\n", cnwn_get_error());
//         for (int i = 0; i < erf.header.num_entries; i++) {
//             cnwn_path_cat(path, sizeof(path), clean_output_dir, erf.entries[i].filename, NULL);
//             ret = cnwn_erf_copy_entry_to_path(&erf, i, input_file, path);
//             if (ret >= 0)
//                 printf("'%s' => '%s' %"PRId64"\n", erf.entries[i].filename, path, ret);
//             else
//                 fprintf(stderr, "ERROR: %s\n", cnwn_get_error());
//         }
//         cnwn_erf_deinit(&erf);
//     } else
//         fprintf(stderr, "ERROR: %s\n", cnwn_get_error());
//     cnwn_file_close(input_file);
// }

// void try_create(const char * path, const char * input_dir)
// {
//     char tmps[CNWN_PATH_MAX_SIZE];
//     char tmpinputdir[CNWN_PATH_MAX_SIZE];
//     cnwn_path_clean(tmpinputdir, sizeof(tmpinputdir), input_dir);
//     cnwn_StringArray contents;
//     cnwn_string_array_init(&contents);
//     int num = cnwn_file_system_ls2(tmpinputdir, false, &contents);
//     cnwn_file_system_ls2(tmpinputdir, false, &contents);
//     if (num >= 0) {
//         cnwn_ERFHeader header = {0};
//         int num_entries = cnwn_array_get_length(&contents);
//         cnwn_ERFEntry * entries = malloc(sizeof(cnwn_ERFEntry) * num_entries);
//         memset(entries, 0, sizeof(cnwn_ERFEntry) * num_entries);
//         int ret = cnwn_erf_header_set_from_filenames(&header, entries, CNWN_RESOURCE_TYPE_MOD, NULL, &contents);
//         if (ret >= 0) {
//             printf("Type: %s\n", header.typestr);
//             printf("Version: %s\n", header.versionstr);
//             printf("Localized strings count: %d\n", header.num_localized_strings);
//             printf("Localized strings offset: 0x%x\n", header.localized_strings_offset);
//             printf("Localized strings size: %d\n", header.localized_strings_size);
//             printf("Entry count: %d\n", header.num_entries);
//             printf("Keys offset: 0x%x\n", header.keys_offset);
//             printf("Values offset: 0x%x\n", header.values_offset);
//             for (int i = 0; i < ret; i++) {
//                 printf("  %d: 0x%x '%s' %u bytes (key=%x, value=%x)\n", i, entries[i].offset, entries[i].filename, entries[i].size, entries[i].key_offset, entries[i].value_offset);
//             }
//             cnwn_File * output_f = cnwn_file_open(path, "wt");
//             if (output_f != NULL) {
//                 int64_t dret = cnwn_erf_header_write(&header, output_f);
//                 if (dret >= 0) {
//                     dret = cnwn_erf_header_write_entries(&header, output_f, entries);
//                     if (dret >= 0) {
//                         for (int i = 0; i < ret; i++) {
//                             printf("%d '%s'\n", i, entries[i].key);
//                             dret = 0; //cnwn_erf_entry_copy_from_path(entries + i, entries[i].filename, output_f);
//                             if (dret >= 0)
//                                 printf("%s <= '%s' %"PRId64"\n", entries[i].key, entries[i].filename, dret);
//                             else
//                                 fprintf(stderr, "ERROR: %s\n", cnwn_get_error());
//                         }
//                     } else
//                         fprintf(stderr, "ERROR: %s\n", cnwn_get_error());
//                 } else
//                     fprintf(stderr, "ERROR: %s\n", cnwn_get_error());
//                 cnwn_file_close(output_f);
//             } else
//                 fprintf(stderr, "ERROR: %s\n", cnwn_get_error());
//         } else
//             fprintf(stderr, "ERROR: %s\n", cnwn_get_error());
//         free(entries);
//     } else
//         fprintf(stderr, "ERROR: %s\n", cnwn_get_error());
//     cnwn_array_deinit(&contents);
// }

int main(int argc, char * argv[])
{
    try_info(argc > 1 ? argv[1] : "../tests/test.mod");
    // try_extract(argc > 1 ? argv[1] : "../tests/test.mod", "tmp");
    // try_create(argc > 1 ? argv[1] : "tmp.mod", "tmp");
    // cnwn_ERF erf = {0};
    // int64_t ret = cnwn_erf_init_from_info_path(&erf, "erf-info");
    // printf("Ret: %"PRId64"\n", ret);
    // printf("Type: %d\n", erf.header.type);
    // printf("Version: %d.%d\n", erf.header.version.major, erf.header.version.minor);
    // printf("Description strref: %u\n", erf.header.description_strref);
    // printf("Year: %u\n", erf.header.year);
    // printf("Day of year: %u\n", erf.header.day_of_year);
    // int lslen = cnwn_array_get_length(&erf.localized_strings);
    // for (int i = 0; i < lslen; i++) {
    //     const cnwn_LocalizedString * ls = cnwn_localized_string_array_get(&erf.localized_strings, i);
    //     printf("String #%d %d '%s'\n", i, ls->language_id, ls->text);
    // }
    // cnwn_erf_deinit(&erf);
    // return 0;
}

#include "cnwn/containers.h"
#include "cnwn/resource/erf.h"

int main(int argc, char * argv[])
{
    cnwn_ERFEntryArray array;
    cnwn_erf_entry_array_init(&array);
    cnwn_StringArray filenames;
    cnwn_string_array_init(&filenames);
    for (int i = 1; i < argc; i++)
        cnwn_string_array_append(&filenames, "%s", argv[i]);         
    cnwn_erf_entry_array_append_files(&array, &filenames);
    cnwn_array_deinit(&filenames);
    cnwn_array_remove(&array, 1, 1);
    cnwn_array_remove(&array, 1, 1);
    for (int i = 0; i < cnwn_array_get_length(&array); i++) {
        cnwn_ERFEntry * entry = cnwn_erf_entry_array_get(&array, i);
        printf("Entry %d = '%s'\n", i, entry->path);
    }
    cnwn_array_deinit(&array);
    return 0;
}

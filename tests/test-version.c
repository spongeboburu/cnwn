#include "cnwn/version.h"

void test_version_parse(const char * s)
{
    cnwn_Version version = cnwn_version_parse(s);
    printf("'%s' => %d.%d\n", s, version.major, version.minor);
}

int main(int argc, char * argv[])
{
    test_version_parse("V1");
    test_version_parse("V9");
    test_version_parse("V1.2");
    test_version_parse("V4.5");
    test_version_parse("V123.456");
    test_version_parse("123.456");
    return 0;
}

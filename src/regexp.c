#include "cnwn/regexp.h"

#include <regex.h>
struct cnwn_Regexp_s { regex_t regex; };

static void cnwn_regexp_array_init_elements(void * elements, int length, const void * source)
{
    if (source != NULL) {
        for (int i = 0; i < length; i++) {
            *(((cnwn_Regexp *)elements) + i) = *(((const cnwn_Regexp *)elements) + i);
        }
    } else
        memset(elements, 0, sizeof(cnwn_Regexp) * length);
}

static void cnwn_regexp_array_deinit_elements(void * elements, int length)
{
    for (int i = 0; i < length; i++) 
        cnwn_regexp_deinit(((cnwn_Regexp *)elements) + i);
}

const cnwn_ContainerCallbacks CNWN_REGEXP_ARRAY_FUNCTIONS = {
    &cnwn_regexp_array_init_elements,
    &cnwn_regexp_array_deinit_elements,
    NULL
};

////////////////////////////////////////////////////////////////
//
//
// Regexp.
// 
//
////////////////////////////////////////////////////////////////

int cnwn_regexp_init(cnwn_Regexp * regexp, const char * re)
{
    memset(regexp, 0, sizeof(cnwn_Regexp));
    regex_t regex;
    int ret = regcomp(&regex, re, REG_ICASE);
    if (ret != 0) {
        char tmps[1024];
        regerror(ret, &regex, tmps, sizeof(tmps));
        cnwn_set_error("\"%s\": %s", re, tmps);
        return -1;
    }    
    regexp->regex = regex;
    return 0;
}
        
void cnwn_regexp_deinit(cnwn_Regexp * regexp)
{
    regfree(&regexp->regex);
}

bool cnwn_regexp_match(const cnwn_Regexp * regexp, const char * s)
{
    int ret = regexec(&regexp->regex, s, 0, NULL, 0);
    return ret == 0;
}

////////////////////////////////////////////////////////////////
//
//
// Regexp array.
// 
//
////////////////////////////////////////////////////////////////

void cnwn_regexp_array_init(cnwn_RegexpArray * regexp_array)
{
    cnwn_array_init(regexp_array, sizeof(cnwn_Regexp), &CNWN_REGEXP_ARRAY_FUNCTIONS);
}

int cnwn_regexp_array_init_from_strings(cnwn_RegexpArray * regexp_array, const cnwn_StringArray * string_array)
{
    cnwn_regexp_array_init(regexp_array);
    int num = cnwn_array_get_length(string_array);
    if (num > 0) {
        cnwn_array_set_length(regexp_array, num);
        for (int i = 0; i < num; i++) {
            cnwn_Regexp regexp = {0};
            const char * s = cnwn_string_array_get(string_array, i);
            if (cnwn_regexp_init(&regexp, s) < 0) {
                cnwn_array_deinit(regexp_array);
                return -1;
            }
            ((cnwn_Regexp *)regexp_array->data)[i] = regexp;
        }
    }
    return num;
}

int cnwn_regexp_array_append(cnwn_RegexpArray * regexp_array, const char * re)
{
    cnwn_Regexp regexp = {0};
    if (cnwn_regexp_init(&regexp, re) < 0) {
        cnwn_array_deinit(regexp_array);
        return -1;
    }
    return cnwn_array_append(regexp_array, 1, &regexp);
}

bool cnwn_regexp_array_match_all(const cnwn_RegexpArray * regexp_array, const char * s)
{
    for (int i = 0; i < regexp_array->length; i++)
        if (!cnwn_regexp_match(((const cnwn_Regexp *)regexp_array->data) + i, s))
            return false;
    return true;
}

bool cnwn_regexp_array_match_any(const cnwn_RegexpArray * regexp_array, const char * s)
{
    for (int i = 0; i < regexp_array->length; i++)
        if (cnwn_regexp_match(((const cnwn_Regexp *)regexp_array->data) + i, s))
            return true;
    return regexp_array->length <= 0;
}

#include "cnwn/version.h"

cnwn_Version cnwn_version_parse(const char * s)
{
    cnwn_Version ret = {0};
    if (s != NULL) {
        char major_part[16] = {0};
        char minor_part[16] = {0};
        if (s[0] == 'v' || s[0] == 'V')
            s++;
        int length = 0;
        for (int i = 0; i < 16; i++)
            if (s[i] > 32)
                length++;
            else
                break;
        int dot_index = -1;
        for (int i = 0; i < length; i++)
            if (s[i] == '.') {
                dot_index = i;
                break;
            }
        if (dot_index < 0) {
            for (int i = 0; i < length - 1; i++)
                if (s[i] > 32)
                    major_part[i] = s[i];
                else
                    break;
        } else {
            for (int i = 0; i < dot_index; i++)
                if (s[i] > 32)
                    major_part[i] = s[i];
                else
                    break;
            for (int i = 0; i < length - dot_index - 1; i++)
                if (s[i] > 32)
                    minor_part[i] = s[i + dot_index + 1];
                else
                    break;
        }
        cnwn_strint(major_part, 10, &ret.major);
        cnwn_strint(minor_part, 10, &ret.minor);
    }
    return ret;
}


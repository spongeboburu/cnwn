#include "cnwn/tools/cnwn-erf.h"

int main(int argc, char * argv[])
{
    cnwn_ERFToolSettings settings;
    int error_index;
    int ret = cnwn_erf_tool_settings_init_from_arguments(&settings, argc, argv, &error_index);
    if (ret < 0) {
        switch (ret) {
        case CNWN_OPTION_ERROR_INVALID:
            fprintf(stderr, "Invalid option at %d: %s\n", error_index, argv[error_index]);
            break;
        case CNWN_OPTION_ERROR_NOARG:
            fprintf(stderr, "Missing argument at %d: %s\n", error_index, argv[error_index]);
            break;
        default:
            fprintf(stderr, "Unknown error at %d: %s\n", error_index, argv[error_index]);
            break;
        }
        return 1;
    } else if (settings.help) {
        printf("cnwn-erf [options] <command> <ERF file> [args]\n");
        printf("%d.%d.%d\n", BUILD_VERSION_MAJOR, BUILD_VERSION_MINOR, BUILD_VERSION_PATCH);
        printf("  General options:\n");
        cnwn_options_print(CNWN_ERF_TOOL_OPTIONS_GENERAL, "    ", stdout);
        printf("\n");
        printf("  list [options] <ERF file> [regular expressions]\n");
        cnwn_options_print(CNWN_ERF_TOOL_OPTIONS_LIST, "    ", stdout);
        printf("\n");
        printf("  create [options] <ERF file> [files]\n");
        cnwn_options_print(CNWN_ERF_TOOL_OPTIONS_CREATE, "    ", stdout);
        printf("\n");
        printf("  extract [options] <ERF file> [regular expressions]\n");
        cnwn_options_print(CNWN_ERF_TOOL_OPTIONS_EXTRACT, "    ", stdout);
        printf("\n");
        return 0;
    } else if (settings.version) {
        printf("%d.%d.%d\n", BUILD_VERSION_MAJOR, BUILD_VERSION_MINOR, BUILD_VERSION_PATCH);
        return 0;
    }
    ret = cnwn_erf_tool_execute(&settings);
    if (ret < 0)
        fprintf(stderr, "ERROR: %s\n", cnwn_get_error());
    cnwn_erf_tool_settings_deinit(&settings);
    return 0;
}

#include "cnwn/tools/cnwn-erf.h"

const cnwn_Option CNWN_ERF_TOOL_OPTIONS_GENERAL[] = {
    {'h', "help", NULL, "Show this help and exit.", CNWN_ERF_TOOL_OPTION_HELP},
    {'V', "show-version", NULL, "Show version and exit.", CNWN_ERF_TOOL_OPTION_VERSION},
    {0}
};

const cnwn_Option CNWN_ERF_TOOL_OPTIONS_LIST[] = {
    {'i', "info", NULL, "Show ERF info at top.", CNWN_ERF_TOOL_OPTION_INFO},
    {'l', "localized-strings", NULL, "List localized strings.", CNWN_ERF_TOOL_OPTION_LOCSTRINGS},
    {'S', "summary", NULL, "Show summary at end.", CNWN_ERF_TOOL_OPTION_SUMMARY},
    {'s', "size", NULL, "Show entry sizes.", CNWN_ERF_TOOL_OPTION_SIZE},
    {'O', "offset", NULL, "Show entry offsets.", CNWN_ERF_TOOL_OPTION_OFFSET},
    {'n', "no-entries", NULL, "Don't list entries.", CNWN_ERF_TOOL_OPTION_NOENTRIES},
    {0}
};
    
const cnwn_Option CNWN_ERF_TOOL_OPTIONS_CREATE[] = {
    {'M', "no-meta", NULL, "Ignore erf-meta file.", CNWN_ERF_TOOL_OPTION_NOMETA},
    {'v', "set-version", "VERSION", "Set version.", CNWN_ERF_TOOL_OPTION_SETVERSION},
    {'y', "set-year", "YEAR", "Set year.", CNWN_ERF_TOOL_OPTION_SETYEAR},
    {'d', "set-day-of-year", "DOY", "Set day of year.", CNWN_ERF_TOOL_OPTION_SETDOY},
    {'q', "quiet", NULL, "Supress output to stdout.", CNWN_ERF_TOOL_OPTION_QUIET},
    {0}
};

const cnwn_Option CNWN_ERF_TOOL_OPTIONS_EXTRACT[] = {
    {'M', "no-meta", NULL, "Do not extract erf-meta file.", CNWN_ERF_TOOL_OPTION_NOMETA},
    {'S', "summary", NULL, "Show summary at end.", CNWN_ERF_TOOL_OPTION_SUMMARY},
    {'s', "size", NULL, "Show entry sizes.", CNWN_ERF_TOOL_OPTION_SIZE},
    {'o', "output", "DIR", "Extract to this directory.", CNWN_ERF_TOOL_OPTION_OUTPUT},
    {'q', "quiet", NULL, "Supress output to stdout.", CNWN_ERF_TOOL_OPTION_QUIET},
    {0}
};


void cnwn_erf_tool_settings_init(cnwn_ERFToolSettings * settings)
{
    memset(settings, 0, sizeof(cnwn_ERFToolSettings));
    cnwn_string_array_init(&settings->arguments);
}

int cnwn_erf_tool_settings_init_from_arguments(cnwn_ERFToolSettings * settings, int argc, char * argv[], int * ret_error_index)
{
    cnwn_erf_tool_settings_init(settings);
    const cnwn_Option * current_options = CNWN_ERF_TOOL_OPTIONS_GENERAL;
    int index = 1;
    while (index < argc) {
        cnwn_OptionResult result;
        const cnwn_Option * used_options = current_options;
        int ret = cnwn_options_parse_argument(used_options, index, argc, argv, &result);
        if (ret < 0 && used_options != CNWN_ERF_TOOL_OPTIONS_GENERAL) {
            used_options = CNWN_ERF_TOOL_OPTIONS_GENERAL;
            ret = cnwn_options_parse_argument(used_options, index, argc, argv, &result);
        }
        if (ret < 0) {
            if (ret_error_index != NULL)
                *ret_error_index = result.index;
            cnwn_erf_tool_settings_deinit(settings);
            return ret;
        } else if (ret == 0)
            break;
        if (result.optindex >= 0) {
            const cnwn_Option * option = used_options + result.optindex;
            switch (option->optvalue) {
            case CNWN_ERF_TOOL_OPTION_HELP:
                settings->help = true;
                return index;
            case CNWN_ERF_TOOL_OPTION_VERSION:
                settings->version = true;
                return index;
            case CNWN_ERF_TOOL_OPTION_INFO:
                settings->info = true;
                break;
            case CNWN_ERF_TOOL_OPTION_LOCSTRINGS:
                settings->locstrings = true;
                break;
            case CNWN_ERF_TOOL_OPTION_SUMMARY:
                settings->summary = true;
                break;
            case CNWN_ERF_TOOL_OPTION_SIZE:
                settings->size = true;
                break;
            case CNWN_ERF_TOOL_OPTION_OFFSET:
                settings->offset = true;
                break;
            case CNWN_ERF_TOOL_OPTION_NOENTRIES:
                settings->noentries = true;
                break;
            case CNWN_ERF_TOOL_OPTION_SETVERSION:
                settings->setversion = argv[result.index + 1];
                break;
            case CNWN_ERF_TOOL_OPTION_SETYEAR:
                settings->setyear = argv[result.index + 1];
                break;
            case CNWN_ERF_TOOL_OPTION_SETDOY:
                settings->setdoy = argv[result.index + 1];
                break;
            case CNWN_ERF_TOOL_OPTION_QUIET:
                settings->quiet = true;
                break;
            case CNWN_ERF_TOOL_OPTION_OUTPUT:
                settings->output = argv[result.index + 1];
                break;
            case CNWN_ERF_TOOL_OPTION_NOMETA:
                settings->nometa = true;
                break;
            default:
                break;
            }
        } else if (settings->command == CNWN_ERF_TOOL_COMMAND_NONE) {
            if (cnwn_strstartswith("list", argv[result.index]) || cnwn_strcmp("ls", argv[result.index]) == 0) {
                settings->command = CNWN_ERF_TOOL_COMMAND_LIST;
                current_options = CNWN_ERF_TOOL_OPTIONS_LIST;
            } else if (cnwn_strstartswith("create", argv[result.index])) {
                settings->command = CNWN_ERF_TOOL_COMMAND_CREATE;
                current_options = CNWN_ERF_TOOL_OPTIONS_CREATE;
            } else if (cnwn_strstartswith("extract", argv[result.index]) || cnwn_strstartswith("xtract", argv[result.index])) {
                settings->command = CNWN_ERF_TOOL_COMMAND_EXTRACT;
                current_options = CNWN_ERF_TOOL_OPTIONS_EXTRACT;
            } else {
                settings->command = CNWN_ERF_TOOL_COMMAND_INVALID;
                return result.index;
            }
        } else if (settings->erf_file == NULL) {
            settings->erf_file = cnwn_strdup(argv[result.index]);
        } else
            cnwn_string_array_append(&settings->arguments, argv[result.index]);
        index += ret;
    }
    if (ret_error_index != NULL)
        *ret_error_index = -1;
    return index;
}

void cnwn_erf_tool_settings_deinit(cnwn_ERFToolSettings * settings)
{
    if (settings->erf_file != NULL)
        free(settings->erf_file);
    cnwn_array_deinit(&settings->arguments);
}


int cnwn_erf_tool_execute(const cnwn_ERFToolSettings * settings)
{
    switch (settings->command) {
    case CNWN_ERF_TOOL_COMMAND_NONE:
        cnwn_set_error("missing command");
        return -1;
    case CNWN_ERF_TOOL_COMMAND_LIST:
        return cnwn_erf_tool_list(settings);
    case CNWN_ERF_TOOL_COMMAND_CREATE:
        return cnwn_erf_tool_create(settings);
    case CNWN_ERF_TOOL_COMMAND_EXTRACT:
        return cnwn_erf_tool_extract(settings);
    default:
        break;
    }
    cnwn_set_error("unknown command");
    return -1;
}

int cnwn_erf_tool_list(const cnwn_ERFToolSettings * settings)
{
    if (settings->command != CNWN_ERF_TOOL_COMMAND_LIST) {
        cnwn_set_error("command mismatch");
        return -1;
    }
    if (cnwn_strisblank(settings->erf_file)) {
        cnwn_set_error("no ERF file specified");
        return -1;
    }
    cnwn_File * input_file = cnwn_file_open(settings->erf_file, "r");
    if (input_file == NULL) {
        cnwn_set_error("%s: %s", cnwn_get_error(), settings->erf_file != NULL ? settings->erf_file : "");
        return -1;
    }
    cnwn_ERFHeader header = {0};
    int64_t ret = cnwn_erf_header_read(&header, input_file);
    if (ret < 0) {        
        cnwn_set_error("%s: %s", cnwn_get_error(), settings->erf_file);
        cnwn_file_close(input_file);
        return -1;
    }
    if (settings->info) {
        printf("size %"PRId64"\n", cnwn_file_size(input_file));
        printf("type %s\n", header.type);
        printf("version %s\n", header.version);
        printf("localized-strings-count %d\n", header.num_localized_strings);
        printf("localized-strings-offset 0x%x\n", header.localized_strings_offset);
        printf("localized-strings-size %d\n", header.localized_strings_size);
        printf("entry-count %d\n", header.num_entries);
        printf("keys-offset 0x%x\n", header.keys_offset);
        printf("values-offset 0x%x\n", header.values_offset);
        printf("description-sttref %u\n", header.description_strref);
        printf("year %u\n", header.year + 1900);
        printf("day-of-year %u\n", header.day_of_year + 1);
    }
    if (settings->locstrings && header.num_localized_strings > 0 && header.localized_strings_size > 0) {
        cnwn_LocalizedStringArray localized_strings;
        cnwn_localized_string_array_init(&localized_strings);
        int64_t rret = cnwn_file_seek(input_file, header.localized_strings_offset);
        if (rret < 0) {
            cnwn_set_error("%s (seeking localized strings)", cnwn_get_error());
            cnwn_array_deinit(&localized_strings);
            cnwn_file_close(input_file);
            return -1;
        }
        rret = cnwn_localized_string_array_read(&localized_strings, header.num_localized_strings, input_file);
        if (rret < 0) {
            cnwn_set_error("%s (reading localized strings)", cnwn_get_error());
            cnwn_array_deinit(&localized_strings);
            cnwn_file_close(input_file);
            return -1;
        }
        int num = cnwn_array_get_length(&localized_strings);
        for (int i = 0; i < num; i++) {
            const cnwn_LocalizedString * ls = cnwn_localized_string_array_get(&localized_strings, i);
            int len = cnwn_strescapectl(NULL, 0, ls->text);
            char * tmps = malloc(sizeof(char) * (len + 1));
            cnwn_strescapectl(tmps, len + 1, ls->text);
            printf("%03u %s\n", ls->language_id, tmps);
            free(tmps);
        }
        cnwn_array_deinit(&localized_strings);
    }
    cnwn_RegexpArray regexps;
    if (cnwn_regexp_array_init_from_strings(&regexps, &settings->arguments) < 0) {
        cnwn_set_error("invalid regexp: %s", cnwn_get_error());
        cnwn_file_close(input_file);
        return -1;
    }
    cnwn_ERFEntryArray entries;
    cnwn_erf_entry_array_init(&entries);
    if (header.num_entries > 0) {
        int64_t rret = cnwn_erf_entry_array_read(&entries, &header, input_file);
        if (rret < 0) {
            cnwn_set_error("%s (reading entries)", cnwn_get_error());
            cnwn_array_deinit(&entries);
            cnwn_array_deinit(&regexps);
            cnwn_file_close(input_file);
            return -1;
        }
    }
    int num = cnwn_array_get_length(&entries);
    int64_t resource_size = 0;
    int num_entries = 0;
    for (int i = 0; i < num; i++) {
        cnwn_ERFEntry * entry = cnwn_erf_entry_array_get(&entries, i);
        if (cnwn_regexp_array_match_any(&regexps, entry->path)) {
            resource_size += entry->size;
            num_entries++;
            if (!settings->noentries) {
                printf("%s", entry->path);
                if (settings->size)
                    printf(" %u", entry->size);
                if (settings->offset)
                    printf(" 0x%x", entry->offset);
                printf("\n");
            }
        }
    }
    if (settings->summary)
        printf("%d entries, %"PRId64" bytes\n", num_entries, resource_size);
    cnwn_array_deinit(&entries);
    cnwn_array_deinit(&regexps);
    cnwn_file_close(input_file);
    return 0;
}

int cnwn_erf_tool_create(const cnwn_ERFToolSettings * settings)
{
    if (settings->command != CNWN_ERF_TOOL_COMMAND_CREATE) {
        cnwn_set_error("command mismatch");
        return -1;
    }
    if (cnwn_strisblank(settings->erf_file)) {
        cnwn_set_error("no ERF file specified");
        return -1;
    }
    int num = cnwn_array_get_length(&settings->arguments);
    cnwn_ERFHeader header = {0};
    cnwn_LocalizedStringArray localized_strings;
    cnwn_localized_string_array_init(&localized_strings);
    if (!settings->nometa) {
        for (int i = 0; i < num; i++) {
            const char * path = cnwn_string_array_get(&settings->arguments, i);
            char basepart[CNWN_PATH_MAX_SIZE];
            cnwn_path_basepart(basepart, sizeof(basepart), path);
            if (cnwn_strcmp(basepart, "erf-meta") == 0) {
                cnwn_File * input_file = cnwn_file_open(path, "r");
                if (input_file == NULL) {
                    cnwn_set_error("%s: %s", cnwn_get_error(), path);
                    cnwn_array_deinit(&localized_strings);
                    return -1;
                }
                cnwn_StringArray strings;
                cnwn_string_array_init(&strings);
                int64_t rret = cnwn_file_read_lines(input_file, &strings);
                if (rret < 0) {
                    cnwn_set_error("%s: %s", cnwn_get_error(), path);
                    cnwn_file_close(input_file);
                    cnwn_array_deinit(&strings);
                    cnwn_array_deinit(&localized_strings);
                    return -1;
                }
                cnwn_file_close(input_file);
                int dret = cnwn_erf_header_parse_info_lines(&header, &strings);
                if (dret < 0) {
                    cnwn_set_error("%s: %s", cnwn_get_error(), path);
                    cnwn_array_deinit(&strings);
                    cnwn_array_deinit(&localized_strings);
                    return -1;
                }
                dret = cnwn_localized_string_array_parse_info_lines(&localized_strings, &strings);
                if (dret < 0) {
                    cnwn_set_error("%s: %s", cnwn_get_error(), path);
                    cnwn_array_deinit(&strings);
                    cnwn_array_deinit(&localized_strings);
                    return -1;
                }
                cnwn_array_deinit(&strings);
            }
        }
    }
    if (!cnwn_strisblank(settings->setversion)) {
        cnwn_Version version = cnwn_version_parse(settings->setversion);
        if (!CNWN_ERF_VERSION_VALID(&version)) {
            cnwn_set_error("invalid version: %s\n", settings->setyear);
            cnwn_array_deinit(&localized_strings);
            return -1;
        }
        header.version[0] = 'V';
        header.version[1] = '0' + version.major;
        header.version[2] = '.';
        header.version[3] = '0' + version.minor;
        header.version[4] = 0;
    }
    if (!cnwn_strisblank(settings->setyear)) {
        uint32_t tmp = 0;
        if (!cnwn_struint32(settings->setyear, 10, &tmp)) {
            cnwn_set_error("invalid year: %s\n", settings->setyear);
            cnwn_array_deinit(&localized_strings);
            return -1;
        }
        header.year = tmp;
    }
    if (!cnwn_strisblank(settings->setdoy)) {
        uint32_t tmp = 0;
        if (!cnwn_struint32(settings->setdoy, 10, &tmp)) {
            cnwn_set_error("invalid day-of-year: %s\n", settings->setdoy);
            cnwn_array_deinit(&localized_strings);
            return -1;
        }
        header.day_of_year = tmp;
    }
    cnwn_ERFEntryArray entries;
    cnwn_erf_entry_array_init(&entries);
    int aret = cnwn_erf_entry_array_append_files(&entries, &settings->arguments);
    if (aret < 0) {
        cnwn_array_deinit(&localized_strings);
        cnwn_array_deinit(&entries);
        return -1;
    }
    for (int i = 0; i < cnwn_array_get_length(&entries); i++) {
        cnwn_ERFEntry * entry_a = cnwn_erf_entry_array_get(&entries, i);
        for (int j = i + 1; j < cnwn_array_get_length(&entries); j++) {
            cnwn_ERFEntry * entry_b = cnwn_erf_entry_array_get(&entries, j);
            if (entry_a->type == entry_b->type && cnwn_strcmpi(entry_a->key, entry_b->key) == 0) {
                // FIXME: windows use case insensetive path cmp
                if (cnwn_strcmp(entry_a->path, entry_b->path) == 0) {
                    cnwn_array_remove(&entries, j, 1);
                    i--;
                    break;
                } else {
                    cnwn_set_error("duplicate entries (%d and %d) \"%s\": %s (%s)", i, j, entry_a->key, entry_b->path, entry_a->path);
                    cnwn_array_deinit(&localized_strings);
                    cnwn_array_deinit(&entries);
                    return -1;
                }
            }
        }
    }
    int num_entries = cnwn_array_get_length(&entries);
    header.num_entries = num_entries;
    aret = cnwn_erf_recalculate(&header, &localized_strings, &entries);
    if (aret < 0) {
        cnwn_array_deinit(&localized_strings);
        cnwn_array_deinit(&entries);
        return -1;
    }
    int validate = cnwn_erf_header_validate(&header);
    if (validate < 0) {
        switch (validate) {
        case CNWN_ERF_HEADER_ERROR_TYPE:
            cnwn_set_error("invalid header ERF type");
            break;
        case CNWN_ERF_HEADER_ERROR_VERSION:
            cnwn_set_error("invalid header version");
            break;
        case CNWN_ERF_HEADER_ERROR_MAX_STRINGS:
            cnwn_set_error("invalid header num localized strings");
            break;
        case CNWN_ERF_HEADER_ERROR_STRINGS_OFFSET:
            cnwn_set_error("invalid header localized strings offset");
            break;
        case CNWN_ERF_HEADER_ERROR_STRINGS_SIZE:
            cnwn_set_error("invalid header localized strings size");
            break;
        case CNWN_ERF_HEADER_ERROR_MAX_ENTRIES:
            cnwn_set_error("invalid header num entries");
            break;
        case CNWN_ERF_HEADER_ERROR_KEYS_OFFSET:    
            cnwn_set_error("invalid header keys offset");
            break;
        case CNWN_ERF_HEADER_ERROR_VALUES_OFFSET:
            cnwn_set_error("invalid header values offset");
            break;
        default:
            cnwn_set_error("unknown header error");
            break;
        }
        cnwn_array_deinit(&localized_strings);
        cnwn_array_deinit(&entries);
        return -1;
    }
    for (int i = 0; i < num_entries; i++) {
        cnwn_ERFEntry * entry = cnwn_erf_entry_array_get(&entries, i);
        validate = cnwn_erf_entry_validate(entry, &header);
        if (validate < 0) {
            switch (validate) {
            case CNWN_ERF_HEADER_ERROR_TYPE:
                cnwn_set_error("invalid header ERF type entry #%d: %s", i, entry->path);
                break;
            case CNWN_ERF_HEADER_ERROR_VERSION:
                cnwn_set_error("invalid header version entry #%d: %s", i, entry->path);
                break;
            case CNWN_ERF_HEADER_ERROR_MAX_STRINGS:
                cnwn_set_error("invalid header num localized strings entry #%d: %s", i, entry->path);
                break;
            case CNWN_ERF_HEADER_ERROR_STRINGS_OFFSET:
                cnwn_set_error("invalid header localized strings offset entry #%d: %s", i, entry->path);
                break;
            case CNWN_ERF_HEADER_ERROR_STRINGS_SIZE:
                cnwn_set_error("invalid header localized strings size entry #%d: %s", i, entry->path);
                break;
            case CNWN_ERF_HEADER_ERROR_MAX_ENTRIES:
                cnwn_set_error("invalid header num entries entry #%d: %s", i, entry->path);
                break;
            case CNWN_ERF_HEADER_ERROR_KEYS_OFFSET:    
                cnwn_set_error("invalid header keys offset entry #%d: %s", i, entry->path);
                break;
            case CNWN_ERF_HEADER_ERROR_VALUES_OFFSET:
                cnwn_set_error("invalid header values offset for entry #%d: %s", i, entry->path);
                break;
            case CNWN_ERF_ENTRY_ERROR_KEY:
                cnwn_set_error("invalid key for entry #%d: %s", i, entry->path);
                break;
            case CNWN_ERF_ENTRY_ERROR_KEY_OFFSET:
                cnwn_set_error("invalid key offset for entry #%d: %s", i, entry->path);
                break;
            case CNWN_ERF_ENTRY_ERROR_VALUE_OFFSET:
                cnwn_set_error("invalid value offset for entry #%d: %s", i, entry->path);
                break;
            default:
                cnwn_set_error("unknown header error for entry #%d: %s", i, entry->path);
                break;
            }
            cnwn_array_deinit(&localized_strings);
            cnwn_array_deinit(&entries);
            return -1;
        }
    }
    cnwn_File * output_file = cnwn_file_open(settings->erf_file, "wt");
    if (output_file == NULL) {
        cnwn_set_error("%s: %s", cnwn_get_error(), settings->erf_file);
        cnwn_array_deinit(&localized_strings);
        cnwn_array_deinit(&entries);
        return -1;
    }
    int64_t rret = cnwn_erf_header_write(&header, output_file);
    if (rret < 0) {
        cnwn_set_error("%s: %s", cnwn_get_error(), settings->erf_file);
        cnwn_array_deinit(&localized_strings);
        cnwn_array_deinit(&entries);
        cnwn_file_close(output_file);
        return -1;
    }
    rret = cnwn_localized_string_array_write(&localized_strings, output_file);
    if (rret < 0) {
        cnwn_set_error("%s for localized strings: %s", cnwn_get_error(), settings->erf_file);
        cnwn_array_deinit(&localized_strings);
        cnwn_array_deinit(&entries);
        cnwn_file_close(output_file);
        return -1;
    }
    cnwn_Version version = cnwn_version_parse(header.version);
    for (int i = 0; i < num_entries; i++) {
        cnwn_ERFEntry * entry = cnwn_erf_entry_array_get(&entries, i);
        rret = cnwn_erf_entry_write_key(entry, &version, output_file);
        if (rret < 0) {
            cnwn_set_error("%s for entry key #%d: %s", cnwn_get_error(), i, entry->path);
            cnwn_array_deinit(&localized_strings);
            cnwn_array_deinit(&entries);
            cnwn_file_close(output_file);
            return -1;
        }
    }
    for (int i = 0; i < num_entries; i++) {
        cnwn_ERFEntry * entry = cnwn_erf_entry_array_get(&entries, i);
        rret = cnwn_erf_entry_write_value(entry, output_file);
        if (rret < 0) {
            cnwn_set_error("%s for entry value #%d: %s", cnwn_get_error(), i, entry->path);
            cnwn_array_deinit(&localized_strings);
            cnwn_array_deinit(&entries);
            cnwn_file_close(output_file);
            return -1;
        }
    }
    int64_t resource_size = 0;
    for (int i = 0; i < num_entries; i++) {
        cnwn_ERFEntry * entry = cnwn_erf_entry_array_get(&entries, i);
        cnwn_File * input_file = cnwn_file_open(entry->path, "r");
        if (input_file == NULL) {
            cnwn_set_error("%s for entry #%d: %s", cnwn_get_error(), i, entry->path);
            cnwn_array_deinit(&localized_strings);
            cnwn_array_deinit(&entries);
            cnwn_file_close(output_file);
            return -1;
        }
        rret = cnwn_file_copy(input_file, entry->size, output_file);
        if (rret < 0) {
            cnwn_set_error("%s for entry #%d: %s", cnwn_get_error(), i, entry->path);
            cnwn_array_deinit(&localized_strings);
            cnwn_array_deinit(&entries);
            cnwn_file_close(output_file);
            cnwn_file_close(input_file);
            return -1;
        }
        cnwn_file_close(input_file);
        if (!settings->quiet) {
            printf("%s", entry->path);
            if (settings->size)
                printf(" %"PRId64, rret);
            if (settings->offset)
                printf(" 0x%x", entry->offset);
            printf("\n");
        }
        resource_size += rret;
    }
    cnwn_array_deinit(&localized_strings);
    cnwn_array_deinit(&entries);
    cnwn_file_close(output_file);
    if (!settings->quiet && settings->summary)
        printf("%d entries, %"PRId64" bytes\n", num_entries, resource_size);
    return 0;
}

int cnwn_erf_tool_extract(const cnwn_ERFToolSettings * settings)
{
    if (settings->command != CNWN_ERF_TOOL_COMMAND_EXTRACT) {
        cnwn_set_error("command mismatch");
        return -1;
    }
    if (cnwn_strisblank(settings->erf_file)) {
        cnwn_set_error("no ERF file specified");
        return -1;
    }
    cnwn_File * input_file = cnwn_file_open(settings->erf_file, "r");
    if (input_file == NULL) {
        cnwn_set_error("%s: %s", cnwn_get_error(), settings->erf_file != NULL ? settings->erf_file : "");
        return -1;
    }
    cnwn_ERFHeader header = {0};
    int64_t ret = cnwn_erf_header_read(&header, input_file);
    if (ret < 0) {        
        cnwn_set_error("%s: %s", cnwn_get_error(), settings->erf_file);
        cnwn_file_close(input_file);
        return -1;
    }
    cnwn_RegexpArray regexps;
    if (cnwn_regexp_array_init_from_strings(&regexps, &settings->arguments) < 0) {
        cnwn_set_error("invalid regexp: %s", cnwn_get_error());
        cnwn_file_close(input_file);
        return -1;
    }
    char output_path[CNWN_PATH_MAX_SIZE];
    cnwn_path_clean(output_path, sizeof(output_path), settings->output);
    if (!cnwn_strisblank(output_path)
        && !cnwn_file_system_isdirectory(output_path)
        && cnwn_file_system_mkdir(output_path) < 0) {
        cnwn_file_close(input_file);
        return -1;
    }    
    if (!settings->nometa) {
        cnwn_LocalizedStringArray localized_strings;
        cnwn_localized_string_array_init(&localized_strings);
        if (header.num_localized_strings > 0 && header.localized_strings_size > 0) {
            int64_t rret = cnwn_file_seek(input_file, header.localized_strings_offset);
            if (rret < 0) {
                cnwn_set_error("%s (seeking localized strings)", cnwn_get_error());
                cnwn_array_deinit(&localized_strings);
                cnwn_file_close(input_file);
                return -1;
            }
            rret = cnwn_localized_string_array_read(&localized_strings, header.num_localized_strings, input_file);
            if (rret < 0) {
                cnwn_set_error("%s (reading localized strings)", cnwn_get_error());
                cnwn_array_deinit(&localized_strings);
                cnwn_file_close(input_file);
                return -1;
            }
            int num = cnwn_array_get_length(&localized_strings);
            for (int i = 0; i < num; i++) {
                const cnwn_LocalizedString * ls = cnwn_localized_string_array_get(&localized_strings, i);
                int len = cnwn_strescapectl(NULL, 0, ls->text);
                char * tmps = malloc(sizeof(char) * (len + 1));
                cnwn_strescapectl(tmps, len + 1, ls->text);
                printf("%03u %s\n", ls->language_id, tmps);
                free(tmps);
            }
        }
        char dopath[CNWN_PATH_MAX_SIZE];
        cnwn_path_cat(dopath, sizeof(dopath), output_path, "erf-meta", NULL);
        cnwn_path_clean(dopath, sizeof(dopath), dopath);
        cnwn_File * output_file = cnwn_file_open(dopath, "wt");
        if (output_file == NULL) {
            cnwn_array_deinit(&regexps);
            cnwn_file_close(input_file);
            cnwn_array_deinit(&localized_strings);
            return -1;
        }
        cnwn_StringArray infolines;
        cnwn_string_array_init(&infolines);
        if (cnwn_erf_header_append_info_lines(&header, &infolines) < 0) {
            cnwn_array_deinit(&infolines);
            cnwn_array_deinit(&localized_strings);
            cnwn_file_close(output_file);
            return -1;
        }
        cnwn_localized_string_append_info_lines(&localized_strings, &infolines);
        int num = cnwn_array_get_length(&infolines);
        int64_t thesize = 0;
        for (int i = 0; i < num; i++) {
            const char * s = cnwn_string_array_get(&infolines, i);            
            int64_t rret = cnwn_file_write_string(output_file, s);
            if (rret < 0) {
                cnwn_array_deinit(&infolines);
                cnwn_array_deinit(&localized_strings);
                cnwn_file_close(output_file);
                return -1;
            }
            thesize += rret;
            // uint8_t tmp = '\n';
            // rret = cnwn_file_write(output_file, 1, &tmp);
            // if (rret < 0) {
            //     cnwn_array_deinit(&infolines);
            //     cnwn_array_deinit(&localized_strings);
            //     cnwn_file_close(output_file);
            //     return -1;
            // }
        }
        if (!settings->quiet) {
            printf("%s", dopath);
            if (settings->size)
                printf(" %"PRId64, thesize);
            printf("\n");
        }
        cnwn_array_deinit(&infolines);
        cnwn_array_deinit(&localized_strings);
        cnwn_file_close(output_file);
    }
    cnwn_ERFEntryArray entries;
    cnwn_erf_entry_array_init(&entries);
    if (header.num_entries > 0) {
        int64_t rret = cnwn_erf_entry_array_read(&entries, &header, input_file);
        if (rret < 0) {
            cnwn_set_error("%s (reading entries)", cnwn_get_error());
            cnwn_array_deinit(&entries);
            cnwn_array_deinit(&regexps);
            cnwn_file_close(input_file);
            return -1;
        }
    }
    int num = cnwn_array_get_length(&entries);
    int64_t resource_size = 0;
    int num_entries = 0;
    for (int i = 0; i < num; i++) {
        cnwn_ERFEntry * entry = cnwn_erf_entry_array_get(&entries, i);
        if (cnwn_regexp_array_match_any(&regexps, entry->path)) {
            char dopath[CNWN_PATH_MAX_SIZE];
            cnwn_path_cat(dopath, sizeof(dopath), output_path, entry->path, NULL);
            cnwn_path_clean(dopath, sizeof(dopath), dopath);
            int64_t rret = cnwn_file_seek(input_file, entry->offset);
            if (rret < 0) {
                cnwn_set_error("%s: %s", cnwn_get_error(), dopath);
                cnwn_array_deinit(&entries);
                cnwn_array_deinit(&regexps);
                cnwn_file_close(input_file);
                return -1;
            }
            cnwn_File * output_file = cnwn_file_open(dopath, "wt");
            if (output_file == NULL) {
                cnwn_set_error("%s: %s", cnwn_get_error(), dopath);
                cnwn_array_deinit(&entries);
                cnwn_array_deinit(&regexps);
                cnwn_file_close(input_file);
                return -1;
            }
            if (entry->size > 0) {
                rret = cnwn_file_copy(input_file, entry->size, output_file);
                if (rret < 0) {
                    cnwn_set_error("%s: %s", cnwn_get_error(), dopath);
                    cnwn_array_deinit(&entries);
                    cnwn_array_deinit(&regexps);
                    cnwn_file_close(input_file);
                    cnwn_file_close(output_file);
                    return -1;
                }
            }
            cnwn_file_close(output_file);
            if (!settings->quiet) {
                printf("%s", dopath);
                if (settings->size)
                    printf(" %u", entry->size);
                if (settings->offset)
                    printf(" 0x%x", entry->offset);
                printf("\n");
            }
            resource_size += entry->size;
            num_entries++;
        }
    }
    if (!settings->quiet && settings->summary)
        printf("%d entries, %"PRId64" bytes\n", num_entries, resource_size);
    cnwn_array_deinit(&entries);
    cnwn_array_deinit(&regexps);
    cnwn_file_close(input_file);
    return 0;
}

#include "cnwn/resource/localized_strings.h"

static void cnwn_localized_string_array_init_elements(void * elements, int length, const void * source)
{
    const cnwn_LocalizedString * source_strings = source;
    cnwn_LocalizedString * strings = elements;
    if (source != NULL) {
        for (int i = 0; i < length; i++)
            cnwn_localized_string_init(strings + i, source_strings[i].language_id, source_strings[i].text);
    } else
        memset(strings, 0, sizeof(cnwn_LocalizedString) * length);
}

static void cnwn_localized_string_array_deinit_elements(void * elements, int length)
{
    cnwn_LocalizedString * strings = elements;
    for (int i = 0; i < length; i++)
        cnwn_localized_string_deinit(strings + i);
}

static int cnwn_localized_string_array_compare_elements(const void * element_a, const void * element_b)
{
    const cnwn_LocalizedString * string_a = element_a;
    const cnwn_LocalizedString * string_b = element_b;
    if (string_a == string_b)
        return 0;
    if (string_a != NULL && string_b == NULL)
        return 1;
    if (string_a == NULL && string_b != NULL)
        return -1;
    if (string_a->language_id > string_b->language_id)
        return 1;
    if (string_a->language_id < string_b->language_id)
        return -1;
    return 0;
}


void cnwn_localized_string_init(cnwn_LocalizedString * localized_string, int language_id, const char * text)
{
    memset(localized_string, 0, sizeof(cnwn_LocalizedString));
    localized_string->language_id = language_id;
    localized_string->text = cnwn_strdup(text);
}

int64_t cnwn_localized_string_init_read(cnwn_LocalizedString * localized_string, cnwn_File * input_file)
{
    memset(localized_string, 0, sizeof(cnwn_LocalizedString));
    return cnwn_localized_string_read(localized_string, input_file);
}

int cnwn_localized_string_init_from_info_line(cnwn_LocalizedString * localized_string, const char * s)
{
    memset(localized_string, 0, sizeof(cnwn_LocalizedString));
    return cnwn_localized_string_set_from_info_line(localized_string, s);
}

void cnwn_localized_string_deinit(cnwn_LocalizedString * localized_string)
{
    if (localized_string != NULL && localized_string->text != NULL)
        free(localized_string->text);
}

int64_t cnwn_localized_string_read(cnwn_LocalizedString * localized_string, cnwn_File * input_file)
{
    int64_t bytes = 0;
    uint32_t language_id;
    uint32_t size;
    int64_t ret;
    ret = cnwn_file_readu32le(input_file, &language_id);
    if (ret < 0) {
        cnwn_set_error("%s (%s)", cnwn_get_error(), "reading language id");
        return -1;
    }
    bytes += ret;
    ret = cnwn_file_readu32le(input_file, &size);
    if (ret < 0) {
        cnwn_set_error("%s (%s)", cnwn_get_error(), "reading size");
        return -1;
    }
    bytes += ret;
    if (size > 0) {
        uint8_t * data = malloc(sizeof(uint8_t) * (size + 1));
        ret = cnwn_file_read_fixed(input_file, size, data);
        if (ret < 0) {
            cnwn_set_error("%s (%s)", cnwn_get_error(), "reading text");
            free(data);
            return -1;
        }
        bytes += ret;
        char * text = malloc(sizeof(char) * (size + 1));
        memset(text, 0, sizeof(char) * (size + 1));
        for (int64_t i = 0; i < ret; i++)
            text[i] = data[i];
        free(data);
        cnwn_localized_string_init(localized_string, language_id, text);
        free(text);
    } else
        cnwn_localized_string_init(localized_string, language_id, "");
    return bytes;
}

int64_t cnwn_localized_string_write(const cnwn_LocalizedString * localized_string, cnwn_File * output_file)
{
    int64_t bytes = 0;
    int64_t ret;
    int textlen = cnwn_strlen(localized_string->text);
    ret = cnwn_file_write32le(output_file, localized_string->language_id);
    if (ret < 0) {
        cnwn_set_error("%s (%s)", cnwn_get_error(), "writing language id");
        return -1;
    }
    bytes += ret;
    ret = cnwn_file_write32le(output_file, textlen);
    if (ret < 0) {
        cnwn_set_error("%s (%s)", cnwn_get_error(), "writing size");
        return -1;
    }
    bytes += ret;
    if (textlen > 0) {
        ret = cnwn_file_write_string(output_file, localized_string->text);
        if (ret < 0) {
            cnwn_set_error("%s (%s)", cnwn_get_error(), "writing text");
            return -1;
        }
        bytes += ret;
        // uint8_t tmp = 0;
        // ret = cnwn_file_write(output_file, 1, &tmp);
        // if (ret < 0) {
        //     cnwn_set_error("%s (%s)", cnwn_get_error(), "writing zero terminator");
        //     return -1;
        // }
        // bytes += ret;
    }
    return bytes;
}

int cnwn_localized_string_set_from_info_line(cnwn_LocalizedString * localized_string, const char * s)
{
    if (s == NULL) {
        cnwn_set_error("NULL string");
        return -1;
    }
    cnwn_StringArray splits;
    char * tmp = cnwn_strdup(s);
    cnwn_strnoctl(tmp, cnwn_strlen(s) + 1, tmp);
    cnwn_string_array_init(&splits);    
    int num = cnwn_string_array_split(&splits, tmp, 2, " ", NULL);
    free(tmp);
    if (num < 2) {
        cnwn_set_error("invalid format");
        cnwn_array_deinit(&splits);
        return -1;
    }
    const char * str = cnwn_string_array_get(&splits, 0);
    if (cnwn_strcmpi(str, "string") != 0) {
        cnwn_set_error("missing String prefix \"%s\"", str);
        cnwn_array_deinit(&splits);
        return -1;
    }
    const char * lang = cnwn_string_array_get(&splits, 1);
    uint32_t lang_id;
    if (!cnwn_struint32(lang, 10, &lang_id)) {
        cnwn_set_error("invalid language ID \"%s\"", lang);
        cnwn_array_deinit(&splits);
        return -1;
    }
    const char * text = cnwn_string_array_get(&splits, 2);
    if (localized_string != NULL) {
        localized_string->language_id = lang_id;
        int tlen = cnwn_strunescapectl(NULL, 0, text);
        localized_string->text = realloc(localized_string->text, sizeof(char) * (tlen + 1));
        cnwn_strunescapectl(localized_string->text, tlen + 1, text);
        cnwn_array_deinit(&splits);
        return tlen;
    }
    cnwn_array_deinit(&splits);
    return cnwn_strlen(text);
}

int cnwn_localized_string_get_info_line(const cnwn_LocalizedString * localized_string, int max_size, char * ret_s)
{
    char lang[128];
    int langlen = snprintf(lang, sizeof(lang), "String %u ", localized_string->language_id);
    int textlen = cnwn_strescapectl(NULL, 0, localized_string->text);
    int newlen = langlen + textlen;
    if (max_size > 0 && newlen > max_size - 1)
        newlen = max_size - 1;
    if (ret_s != NULL && max_size > 0) {
        int offset = 0;
        int copylen = CNWN_MINMAX(langlen, 0, max_size - offset);
        if (copylen > 0)
            memcpy(ret_s + offset, lang, sizeof(char) * langlen);
        offset += copylen;
        if (offset < max_size - 1)
            offset += cnwn_strescapectl(ret_s + offset, max_size - offset, localized_string->text);
        ret_s[offset] = 0;
        return offset;
    }
    return newlen;
}

char * cnwn_localized_string_get_info_line_realloc(const cnwn_LocalizedString * localized_string, char * r)
{
    int len = cnwn_localized_string_get_info_line(localized_string, 0, NULL);
    char * ret = realloc(r, sizeof(char) * (len + 1));
    cnwn_localized_string_get_info_line(localized_string, len + 1, ret);
    return ret;
}

////////////////////////////////////////////////////////////////
//
//
// Array
//
//
////////////////////////////////////////////////////////////////

void cnwn_localized_string_array_init(cnwn_LocalizedStringArray * array)
{
    cnwn_ContainerCallbacks cb = {&cnwn_localized_string_array_init_elements,
                                  &cnwn_localized_string_array_deinit_elements,
                                  &cnwn_localized_string_array_compare_elements};
    cnwn_array_init(array, sizeof(cnwn_LocalizedString), &cb);
}

int64_t cnwn_localized_string_array_read(cnwn_LocalizedStringArray * array, int num, cnwn_File * input_file)
{
    cnwn_localized_string_array_init(array);
    int64_t ret = 0;
    if (num > 0) {
        int current = array->length;
        cnwn_array_set_length(array, current + num);
        for (int i = 0; i < num; i++) {
            int64_t r = cnwn_localized_string_init_read(((cnwn_LocalizedString *)array->data) + current + i, input_file);
            if (r < 0) {
                cnwn_set_error("%s (#%d)", cnwn_get_error(), i);
                cnwn_array_set_length(array, current);
                return -1;
            }
            ret += r;
        }
    }
    return ret;    
}

int64_t cnwn_localized_string_array_write(const cnwn_LocalizedStringArray * array, cnwn_File * output_file)
{
    int64_t ret = 0;
    for (int i = 0; i < array->length; i++) {
        int64_t rret = cnwn_localized_string_write(((const cnwn_LocalizedString *)array->data) + i, output_file);
        if (rret < 0) {
            cnwn_set_error("%s (#%d)", cnwn_get_error(), i);
            return -1;
        }
        ret += rret;
    }
    return ret;
}

int cnwn_localized_string_array_parse_info_lines(cnwn_LocalizedStringArray * array, const cnwn_StringArray * strings)
{
    int ret = 0;
    int num = cnwn_array_get_length(strings);
    if (num > 0) {
        int current = cnwn_array_get_length(array);
        cnwn_array_append(array, num, NULL);
        for (int i = 0; i < num; i++) {
            cnwn_LocalizedString * ls = ((cnwn_LocalizedString *)array->data) + current + ret;
            const char * s = cnwn_string_array_get(strings, i);
            if (!cnwn_strisblank(s) && cnwn_strstartswithi(s, "string")) {
                if (cnwn_localized_string_init_from_info_line(ls, s) < 0) {
                    cnwn_set_error("%s (line %d)", cnwn_get_error(), i);
                    cnwn_array_set_length(array, current);
                    return -1;
                }
                ret++;
            }
        }
        cnwn_array_set_length(array, current + ret);
    }
    return ret;
}

int cnwn_localized_string_append_info_lines(const cnwn_LocalizedStringArray * array, cnwn_StringArray * strings)
{
    int num = cnwn_array_get_length(array);
    int current = cnwn_array_get_length(strings);
    cnwn_array_append(strings, num, NULL);
    for (int i = 0; i < num; i++) {
        const cnwn_LocalizedString * ls = cnwn_localized_string_array_get(array, i);
        char * s = cnwn_localized_string_get_info_line_realloc(ls, NULL);
        cnwn_string_array_set(strings, current + i, s);
        free(s);
    }
    return num;
}

const cnwn_LocalizedString * cnwn_localized_string_array_get(const cnwn_LocalizedStringArray * array, int index)
{
    return cnwn_array_element_ptr(array, index);
}

void cnwn_localized_string_array_append(cnwn_LocalizedStringArray * array, int language_id, const char * s)
{
    cnwn_LocalizedString ls = {language_id, (char *)s};
    cnwn_array_append(array, 1, &ls);
}

void cnwn_localized_string_array_insert(cnwn_LocalizedStringArray * array, int index, int language_id, const char * s)
{
    cnwn_LocalizedString ls = {language_id, (char *)s};
    cnwn_array_insert(array, index, 1, &ls);
}

void cnwn_localized_string_array_set(cnwn_LocalizedStringArray * array, int index, int language_id, const char * s)
{
    cnwn_LocalizedString ls = {language_id, (char *)s};
    cnwn_array_set(array, index, 1, &ls);
}

int cnwn_localized_string_array_append_info_line(cnwn_LocalizedStringArray * array, const char * s)
{
    cnwn_LocalizedString ls;
    int ret = cnwn_localized_string_init_from_info_line(&ls, s);
    if (ret < 0)
        return -1;
    cnwn_array_append(array, 1, &ls);
    cnwn_localized_string_deinit(&ls);
    return ret;
}

int cnwn_localized_string_array_insert_info_line(cnwn_LocalizedStringArray * array, int index, const char * s)
{
    cnwn_LocalizedString ls;
    int ret = cnwn_localized_string_init_from_info_line(&ls, s);
    if (ret < 0)
        return -1;
    cnwn_array_insert(array, index, 1, &ls);
    cnwn_localized_string_deinit(&ls);
    return ret;
}

int cnwn_localized_string_array_set_info_line(cnwn_LocalizedStringArray * array, int index, const char * s)
{
    cnwn_LocalizedString ls;
    int ret = cnwn_localized_string_init_from_info_line(&ls, s);
    if (ret < 0)
        return -1;
    cnwn_array_set(array, index, 1, &ls);
    cnwn_localized_string_deinit(&ls);
    return ret;
}

int cnwn_localized_string_array_find(const cnwn_LocalizedStringArray * array, int language_id)
{
    cnwn_LocalizedString ls = {language_id, NULL};
    return cnwn_array_find(array, 0, false, &ls);
}

uint32_t cnwn_localized_string_get_binary_size(const cnwn_LocalizedStringArray * array)
{
    // FIXME: this could overflow if > UINT32_MAX.
    uint32_t ret = 0;
    int len = cnwn_array_get_length(array);
    for (int i = 0; i < len; i++) {
        const cnwn_LocalizedString * ls = cnwn_localized_string_array_get(array, i);
        if (ls != NULL) {
            ret += cnwn_strlen(ls->text);
            ret += 8;
        }
    }
    return ret;
}

#include "cnwn/resource/erf.h"

static void cnwn_erf_entry_array_init_elements(void * elements, int length, const void * source)
{
    if (source != NULL)
        for (int i = 0; i < length; i++) {
        }
    else
        memset(elements, 0, sizeof(cnwn_ERFEntry) * length);
}

static void cnwn_erf_entry_array_deinit_elements(void * elements, int length)
{
    for (int i = 0; i < length; i++) 
        cnwn_erf_entry_deinit(((cnwn_ERFEntry *)elements) + i);
}

static int cnwn_erf_entry_array_compare_elements(const void * element_a, const void * element_b)
{
    if (element_a == element_b)
        return 0;
    if (element_a == NULL && element_b != NULL)
        return -1;
    if (element_a != NULL && element_b == NULL)
        return 1;
    char tmps_a[CNWN_PATH_MAX_SIZE], tmps_b[CNWN_PATH_MAX_SIZE];
    cnwn_resource_type_to_filename(((const cnwn_ERFEntry *)element_a)->type, ((const cnwn_ERFEntry *)element_a)->key, sizeof(tmps_a), tmps_a);
    cnwn_resource_type_to_filename(((const cnwn_ERFEntry *)element_b)->type, ((const cnwn_ERFEntry *)element_b)->key, sizeof(tmps_b), tmps_b);
    return cnwn_strcmpi(tmps_a, tmps_b);
}

const cnwn_ContainerCallbacks CNWN_ERF_ENTRY_ARRAY_FUNCTIONS = {
    &cnwn_erf_entry_array_init_elements,
    &cnwn_erf_entry_array_deinit_elements,
    &cnwn_erf_entry_array_compare_elements
};

////////////////////////////////////////////////////////////////
//
//
// ERF related functions.
//
//
////////////////////////////////////////////////////////////////

bool cnwn_erf_entry_key_valid(const char * key, const cnwn_Version * version)
{
    if (cnwn_strisblank(key))
        return false;
    if (version == NULL || version->major < 1 || version->minor < 0 || version->minor > 1)
        return false;
    int keylen = CNWN_ERF_KEYLEN(version);
    for (int i = 0; key[i] != 0; i++) 
        if (!CNWN_ERF_ENTRY_NAME_CHAR_VALID(key[i]) || i >= keylen)
            return false;
    return true;
}

////////////////////////////////////////////////////////////////
//
//
// ERF header.
//
//
////////////////////////////////////////////////////////////////

int64_t cnwn_erf_header_read(cnwn_ERFHeader * erf_header, cnwn_File * input_file)
{
    int64_t retbytes = 0;
    cnwn_ERFHeader header = {0};
    int64_t ret = cnwn_file_read_string(input_file, sizeof(header.type), header.type);
    if (ret < 0) {
        cnwn_set_error("%s (reading type)", cnwn_get_error());
        return -1;
    }
    retbytes += ret;
    ret = cnwn_file_read_string(input_file, sizeof(header.version), header.version);
    if (ret < 0) {
        cnwn_set_error("%s (reading version)", cnwn_get_error());
        return -1;
    }
    retbytes += ret;
    ret = cnwn_file_readu32le(input_file, &header.num_localized_strings);
    if (ret < 0) {
        cnwn_set_error("%s (reading num localized strings)", cnwn_get_error());
        return -1;
    }
    retbytes += ret;
    ret = cnwn_file_readu32le(input_file, &header.localized_strings_size);
    if (ret < 0) {
        cnwn_set_error("%s (reading localized strings size)", cnwn_get_error());
        return -1;
    }
    retbytes += ret;
    ret = cnwn_file_readu32le(input_file, &header.num_entries);
    if (ret < 0) {
        cnwn_set_error("%s (reading num entries)", cnwn_get_error());
        return -1;
    }
    retbytes += ret;
    ret = cnwn_file_readu32le(input_file, &header.localized_strings_offset);
    if (ret < 0) {
        cnwn_set_error("%s (reading localized strings offset)", cnwn_get_error());
        return -1;
    }
    retbytes += ret;
    ret = cnwn_file_readu32le(input_file, &header.keys_offset);
    if (ret < 0) {
        cnwn_set_error("%s (reading keys offset)", cnwn_get_error());
        return -1;
    }
    retbytes += ret;
    ret = cnwn_file_readu32le(input_file, &header.values_offset);
    if (ret < 0) {
        cnwn_set_error("%s (reading values offset)", cnwn_get_error());
        return -1;
    }
    retbytes += ret;
    ret = cnwn_file_readu32le(input_file, &header.year);
    if (ret < 0) {
        cnwn_set_error("%s (reading year)", cnwn_get_error());
        return -1;
    }
    retbytes += ret;
    ret = cnwn_file_readu32le(input_file, &header.day_of_year);
    if (ret < 0) {
        cnwn_set_error("%s (reading day of year)", cnwn_get_error());
        return -1;
    }
    retbytes += ret;
    ret = cnwn_file_readu32le(input_file, &header.description_strref);
    if (ret < 0) {
        cnwn_set_error("%s (reading description strref)", cnwn_get_error());
        return -1;
    }
    retbytes += ret;
    ret = cnwn_file_read_fixed(input_file, 116, header.rest);
    if (ret < 0) {
        cnwn_set_error("%s (reading last 116 bytes)", cnwn_get_error());
        return -1;
    }
    retbytes += ret;
    if (erf_header != NULL)
        *erf_header = header;
    return retbytes;
}

int64_t cnwn_erf_header_write(const cnwn_ERFHeader * erf_header, cnwn_File * output_file)
{    
    int64_t retbytes = 0;
    uint8_t tmp[4];
    for (int i = 0; i < 4; i++)
        if (erf_header->type[i] > 32)
            tmp[i] = erf_header->type[i];
        else
            tmp[i] = 32;
    int64_t ret = cnwn_file_write(output_file, 4, tmp);
    if (ret < 0) {
        cnwn_set_error("%s (writing type)", cnwn_get_error());
        return -1;
    }
    retbytes += ret;
    for (int i = 0; i < 4; i++)
        if (erf_header->version[i] > 32)
            tmp[i] = erf_header->version[i];
        else
            tmp[i] = 32;
    ret = cnwn_file_write(output_file, 4, tmp);
    if (ret < 0) {
        cnwn_set_error("%s (writing version)", cnwn_get_error());
        return -1;
    }
    retbytes += ret;
    ret = cnwn_file_writeu32le(output_file, erf_header->num_localized_strings);
    if (ret < 0) {
        cnwn_set_error("%s (writing num localized strings)", cnwn_get_error());
        return -1;
    }
    retbytes += ret;
    ret = cnwn_file_writeu32le(output_file, erf_header->localized_strings_size);
    if (ret < 0) {
        cnwn_set_error("%s (writing localized strings size)", cnwn_get_error());
        return -1;
    }
    retbytes += ret;
    ret = cnwn_file_writeu32le(output_file, erf_header->num_entries);
    if (ret < 0) {
        cnwn_set_error("%s (writing num entries)", cnwn_get_error());
        return -1;
    }
    retbytes += ret;
    ret = cnwn_file_writeu32le(output_file, erf_header->localized_strings_offset);
    if (ret < 0) {
        cnwn_set_error("%s (writing localized strings offset)", cnwn_get_error());
        return -1;
    }
    retbytes += ret;
    ret = cnwn_file_writeu32le(output_file, erf_header->keys_offset);
    if (ret < 0) {
        cnwn_set_error("%s (writing keys offset)", cnwn_get_error());
        return -1;
    }
    retbytes += ret;
    ret = cnwn_file_writeu32le(output_file, erf_header->values_offset);
    if (ret < 0) {
        cnwn_set_error("%s (writing values offset)", cnwn_get_error());
        return -1;
    }
    retbytes += ret;
    ret = cnwn_file_writeu32le(output_file, erf_header->year);
    if (ret < 0) {
        cnwn_set_error("%s (writing year)", cnwn_get_error());
        return -1;
    }
    retbytes += ret;
    ret = cnwn_file_writeu32le(output_file, erf_header->day_of_year);
    if (ret < 0) {
        cnwn_set_error("%s (writing day of year)", cnwn_get_error());
        return -1;
    }
    retbytes += ret;
    ret = cnwn_file_writeu32le(output_file, erf_header->description_strref);
    if (ret < 0) {
        cnwn_set_error("%s (writing description strref)", cnwn_get_error());
        return -1;
    }
    retbytes += ret;
    ret = cnwn_file_write(output_file, 116, erf_header->rest);
    if (ret < 0) {
        cnwn_set_error("%s (writing last 116 bytes)", cnwn_get_error());
        return -1;
    }
    retbytes += ret;
    return retbytes;
}

int cnwn_erf_header_parse_info_lines(cnwn_ERFHeader * erf_header, const cnwn_StringArray * strings)
{
    int num = cnwn_array_get_length(strings);
    int ret = 0;
    cnwn_ERFHeader header = {0};
    for (int i = 0; i < num; i++) {
        const char * ts = cnwn_string_array_get(strings, i);
        char key[128] = {0};
        char value[128] = {0};
        int index = cnwn_strfind(ts, 0, " ", NULL);
        if (index > 0) {
            cnwn_strcpy(key, sizeof(key), ts, index);
            cnwn_strcpy(value, sizeof(value), ts + index + 1, -1);
            cnwn_strnoctl(key, sizeof(key), key);
            cnwn_strstrip(key, sizeof(key), key);
            cnwn_strlower(key, sizeof(key), key);
            cnwn_strnoctl(value, sizeof(value), value);
            cnwn_strstrip(value, sizeof(value), value);
            cnwn_strlower(value, sizeof(value), value);
        }
        if (!cnwn_strisblank(key) || !cnwn_strisblank(value)) {
            if (cnwn_strcmp(key, "type") == 0) {
                cnwn_strupper(header.type, sizeof(header.type), value);
                ret++;
            } else if (cnwn_strcmp(key, "version") == 0) {
                cnwn_strupper(header.version, sizeof(header.version), value);
                ret++;
            } else if (cnwn_strcmp(key, "strref") == 0) {
                if (!cnwn_struint32(value, 10, &header.description_strref)) {
                    cnwn_set_error("Invalid description strref \"%s\" on line %d", value, i);
                    return -1;
                }
                ret++;
            } else if (cnwn_strcmp(key, "year") == 0) {
                if (!cnwn_struint32(value, 10, &header.year)) {
                    cnwn_set_error("Invalid year \"%s\" on line %d", value, i);
                    return -1;
                }
                ret++;
            } else if (cnwn_strcmp(key, "doy") == 0) {
                if (!cnwn_struint32(value, 10, &header.day_of_year)) {
                    cnwn_set_error("Invalid day of year \"%s\" on line %d", value, i);
                    return -1;
                }
                ret++;
            }
        }
    }
    if (erf_header != NULL)
        *erf_header = header;
    return ret;
}

int cnwn_erf_header_append_info_lines(const cnwn_ERFHeader * erf_header, cnwn_StringArray * strings)
{
    char tmps[1024];
    int ret = 0;
    char upperstr[1024];
    cnwn_strupper(upperstr, sizeof(upperstr), erf_header->type);
    snprintf(tmps, sizeof(tmps), "Type %s\n", upperstr);
    ret += cnwn_string_array_append(strings, tmps);
    cnwn_strupper(upperstr, sizeof(upperstr), erf_header->version);
    snprintf(tmps, sizeof(tmps), "Version %s\n", upperstr);
    ret += cnwn_string_array_append(strings, tmps);
    snprintf(tmps, sizeof(tmps), "Strref %u\n", erf_header->description_strref);
    ret += cnwn_string_array_append(strings, tmps);
    snprintf(tmps, sizeof(tmps), "Year %u\n", erf_header->year);
    ret += cnwn_string_array_append(strings, tmps);
    snprintf(tmps, sizeof(tmps), "DOY %u\n", erf_header->day_of_year);
    ret += cnwn_string_array_append(strings, tmps);
    return ret;
}

int cnwn_erf_header_validate(const cnwn_ERFHeader * erf_header)
{
    cnwn_ResourceType rtype = cnwn_resource_type_from_header_string(erf_header->type);
    if (!CNWN_RESOURCE_TYPE_IS_ERF(rtype))
        return CNWN_ERF_HEADER_ERROR_TYPE;
    cnwn_Version version = cnwn_version_parse(erf_header->version);
    int keylen = CNWN_ERF_KEYLEN(&version);
    if (keylen == 0)
        return CNWN_ERF_HEADER_ERROR_VERSION;
    if (erf_header->num_localized_strings > CNWN_ERF_MAX_LOCALIZED_STRINGS)
        return CNWN_ERF_HEADER_ERROR_MAX_STRINGS;
    if (erf_header->localized_strings_offset < CNWN_ERF_HEADER_SIZE)
        return CNWN_ERF_HEADER_ERROR_STRINGS_OFFSET;
    if ((erf_header->num_localized_strings == 0 && erf_header->localized_strings_size != 0)
        || (erf_header->num_localized_strings != 0 && erf_header->localized_strings_size == 0))
        return CNWN_ERF_HEADER_ERROR_STRINGS_SIZE;
    if (erf_header->num_entries > CNWN_ERF_MAX_ENTRIES)
        return CNWN_ERF_HEADER_ERROR_MAX_ENTRIES;
    if (erf_header->keys_offset < erf_header->localized_strings_offset + erf_header->localized_strings_size)
        return CNWN_ERF_HEADER_ERROR_KEYS_OFFSET;    
    if (erf_header->values_offset < erf_header->keys_offset + (keylen + 8) * erf_header->num_entries)
        return CNWN_ERF_HEADER_ERROR_VALUES_OFFSET;
    return 0;
}

////////////////////////////////////////////////////////////////
//
//
// ERF entry.
//
//
////////////////////////////////////////////////////////////////

void cnwn_erf_entry_init(cnwn_ERFEntry * erf_entry)
{
    memset(erf_entry, 0, sizeof(cnwn_ERFEntry));
}

void cnwn_erf_entry_deinit(cnwn_ERFEntry * erf_entry)
{
    if (erf_entry->key != NULL)
        free(erf_entry->key);
    if (erf_entry->path != NULL)
        free(erf_entry->path);
    memset(erf_entry, 0, sizeof(cnwn_ERFEntry));
}

int64_t cnwn_erf_entry_read_key(cnwn_ERFEntry * erf_entry, const cnwn_Version * version, cnwn_File * input_file)
{
    cnwn_Version use_version = {1, 0};
    if (version != NULL)
        use_version = *version;
    int key_size = CNWN_ERF_KEYLEN(&use_version);
    if (key_size == 0) {
        cnwn_set_error("unsupported version %d.%d", use_version.major, use_version.minor);
        return -1;
    }
    cnwn_ERFEntry entry = {0};
    if (erf_entry != NULL)
        entry = *erf_entry;
    int64_t offset = cnwn_file_get_seek(input_file);
    if (offset < 0) {
        cnwn_set_error("%s (getting key offset)", cnwn_get_error());
        return -1;
    }
    if (offset > UINT_MAX) {
        cnwn_set_error("key offset exceeds unsigned 32-bit integer (%"PRId64")", offset);
        return -1;
    }
    entry.key_offset = offset;
    char tmpkey[1024] = {0};
    entry.key = tmpkey;
    int64_t retbytes = 0;
    int64_t ret;
    ret = cnwn_file_read_string(input_file, key_size + 1, entry.key);
    if (ret < 0) {
        cnwn_set_error("%s (reading entry key)", cnwn_get_error());
        return -1;
    }
    retbytes += ret;
    ret = cnwn_file_readu32le(input_file, &entry.id);
    if (ret < 0) {
        cnwn_set_error("%s (reading ID for entry \"%s\")", cnwn_get_error(), entry.key);
        return -1;
    }
    retbytes += ret;
    ret = cnwn_file_readu16le(input_file, &entry.type);
    if (ret < 0) {
        cnwn_set_error("%s (reading type for entry \"%s\")", cnwn_get_error(), entry.key);
        return -1;
    }
    retbytes += ret;
    ret = cnwn_file_readu16le(input_file, &entry.unused);
    if (ret < 0) {
        cnwn_set_error("%s (reading unused for entry \"%s\")", cnwn_get_error(), entry.key);
        return -1;
    }
    retbytes += ret;
    if (erf_entry != NULL) {
        char tmps[CNWN_PATH_MAX_SIZE];
        cnwn_resource_type_to_filename(entry.type, entry.key, sizeof(tmps), tmps);
        entry.key = cnwn_strdup(tmpkey);
        entry.path = cnwn_strdupr(entry.path, tmps);
        *erf_entry = entry;
    }
    return retbytes;
}

int64_t cnwn_erf_entry_write_key(const cnwn_ERFEntry * erf_entry, const cnwn_Version * version, cnwn_File * output_file)
{
    cnwn_Version use_version = {1, 0};
    if (version != NULL)
        use_version = *version;
    int key_size = CNWN_ERF_KEYLEN(&use_version);
    if (key_size == 0) {
        cnwn_set_error("unsupported version %d.%d", use_version.major, use_version.minor);
        return -1;
    }
    cnwn_ERFEntry entry = {0};
    if (erf_entry != NULL)
        entry = *erf_entry;
    int64_t offset = cnwn_file_get_seek(output_file);
    if (offset < 0) {
        cnwn_set_error("%s (getting key offset)", cnwn_get_error());
        return -1;
    }
    if (offset > UINT_MAX) {
        cnwn_set_error("key offset exceeds unsigned 32-bit integer (%"PRId64")", offset);
        return -1;
    }
    entry.key_offset = offset;
    int64_t retbytes = 0;
    int64_t ret;
    uint8_t tmpbytes[64] = {0};
    for (int i = 0; i < key_size && entry.key[i] != 0; i++) 
        tmpbytes[i] = entry.key[i];
    ret = cnwn_file_write(output_file, key_size, tmpbytes);
    if (ret < 0) {
        cnwn_set_error("%s (writing entry key)", cnwn_get_error());
        return -1;
    }
    retbytes += ret;
    ret = cnwn_file_writeu32le(output_file, entry.id);
    if (ret < 0) {
        cnwn_set_error("%s (writing ID for entry \"%s\")", cnwn_get_error(), entry.key);
        return -1;
    }
    retbytes += ret;
    ret = cnwn_file_writeu16le(output_file, entry.type);
    if (ret < 0) {
        cnwn_set_error("%s (writing type for entry \"%s\")", cnwn_get_error(), entry.key);
        return -1;
    }
    retbytes += ret;
    ret = cnwn_file_writeu16le(output_file, entry.unused);
    if (ret < 0) {
        cnwn_set_error("%s (writing unused for entry \"%s\")", cnwn_get_error(), entry.key);
        return -1;
    }
    retbytes += ret;
    return retbytes;
}

int64_t cnwn_erf_entry_read_value(cnwn_ERFEntry * erf_entry, cnwn_File * input_file)
{
    cnwn_ERFEntry entry = {0};
    if (erf_entry != NULL)
        entry = *erf_entry;
    int64_t retbytes = 0;
    int64_t ret;
    int64_t offset = cnwn_file_get_seek(input_file);
    if (offset < 0) {
        cnwn_set_error("%s (getting value offset)", cnwn_get_error());
        return -1;
    }
    if (offset > UINT_MAX) {
        cnwn_set_error("value offset exceeds unsigned 32-bit integer (%"PRId64")", offset);
        return -1;
    }
    entry.value_offset = offset;
    ret = cnwn_file_readu32le(input_file, &entry.offset);
    if (ret < 0) {
        cnwn_set_error("%s (reading offset for entry \"%s\")", cnwn_get_error(), entry.key);
        return -1;
    }
    retbytes += ret;
    ret = cnwn_file_readu32le(input_file, &entry.size);
    if (ret < 0) {
        cnwn_set_error("%s (reading size for entry \"%s\")", cnwn_get_error(), entry.key);
        return -1;
    }
    retbytes += ret;
    if (erf_entry != NULL)
        *erf_entry = entry;
    return retbytes;
}

int64_t cnwn_erf_entry_write_value(const cnwn_ERFEntry * erf_entry, cnwn_File * output_file)
{
    cnwn_ERFEntry entry = {0};
    if (erf_entry != NULL)
        entry = *erf_entry;
    int64_t retbytes = 0;
    int64_t ret;
    int64_t offset = cnwn_file_get_seek(output_file);
    if (offset < 0) {
        cnwn_set_error("%s (getting value offset)", cnwn_get_error());
        return -1;
    }
    if (offset > UINT_MAX) {
        cnwn_set_error("value offset exceeds unsigned 32-bit integer (%"PRId64")", offset);
        return -1;
    }
    entry.value_offset = offset;
    ret = cnwn_file_writeu32le(output_file, entry.offset);
    if (ret < 0) {
        cnwn_set_error("%s (writing offset for entry \"%s\")", cnwn_get_error(), entry.key);
        return -1;
    }
    retbytes += ret;
    ret = cnwn_file_writeu32le(output_file, entry.size);
    if (ret < 0) {
        cnwn_set_error("%s (writing size for entry \"%s\")", cnwn_get_error(), entry.key);
        return -1;
    }
    retbytes += ret;
    return retbytes;
}

int64_t cnwn_erf_entry_set_from_file(cnwn_ERFEntry * erf_entry, const char * path)
{
    int64_t size = cnwn_file_system_size(path, false);
    if (size < 0)
        return -1;
    if (!cnwn_file_system_isfile(path)) {
        cnwn_set_error("not a file");
        return -1;
    }
    if (size > INT32_MAX) {
        cnwn_set_error("file too large");
        return -1;
    }
    cnwn_ResourceType rtype = cnwn_resource_type_from_path(path);
    char tmps[CNWN_PATH_MAX_SIZE];
    cnwn_path_filenamepart(tmps, sizeof(tmps), path);
    erf_entry->type = rtype;
    erf_entry->key = cnwn_strdupr(erf_entry->key, tmps);
    erf_entry->path = cnwn_strdupr(erf_entry->path, path);
    erf_entry->size = size;
    return 0;
}

int cnwn_erf_entry_validate(const cnwn_ERFEntry * erf_entry, const cnwn_ERFHeader * erf_header)
{
    int ret = cnwn_erf_header_validate(erf_header);
    if (ret < 0)
        return ret;
    cnwn_Version version = cnwn_version_parse(erf_header->version);
    int keylen = CNWN_ERF_KEYLEN(&version);
    if (keylen == 0)
        return CNWN_ERF_HEADER_ERROR_VERSION;
    if (!cnwn_erf_entry_key_valid(erf_entry->key, &version))
        return CNWN_ERF_ENTRY_ERROR_KEY;
    //printf("* %u < %u\n", erf_entry->key_offset, erf_header->localized_strings_offset + erf_header->localized_strings_size);
    if ((erf_entry->key_offset < erf_header->localized_strings_offset + erf_header->localized_strings_size)
        || (erf_entry->key_offset >= erf_header->values_offset)
        || (((erf_entry->key_offset - erf_header->keys_offset) % (keylen + 8)) != 0))
        return CNWN_ERF_ENTRY_ERROR_KEY_OFFSET;
    if ((erf_entry->value_offset < erf_header->keys_offset + erf_header->num_entries * (keylen + 8))
        || (erf_entry->value_offset >= erf_header->values_offset + erf_header->num_entries * 8)
        || (((erf_entry->value_offset - erf_header->values_offset) % 8) != 0))
        return CNWN_ERF_ENTRY_ERROR_VALUE_OFFSET;
    return 0;
}

////////////////////////////////////////////////////////////////
//
//
// ERF entry array.
//
//
////////////////////////////////////////////////////////////////

void cnwn_erf_entry_array_init(cnwn_ERFEntryArray * array)
{
    cnwn_array_init(array, sizeof(cnwn_ERFEntry), &CNWN_ERF_ENTRY_ARRAY_FUNCTIONS);
}

int cnwn_erf_entry_array_read(cnwn_ERFEntryArray * array, const cnwn_ERFHeader * erf_header, cnwn_File * input_file)
{
    if (erf_header->num_entries > 0) {
        int64_t ret = cnwn_file_seek(input_file, erf_header->keys_offset);
        if (ret < 0) {
            cnwn_set_error("%s (seeking keys offset %u)", cnwn_get_error(), erf_header->keys_offset);
            return -1;
        }
        cnwn_Version version = cnwn_version_parse(erf_header->version);
        int current = cnwn_array_get_length(array);
        cnwn_array_append(array, erf_header->num_entries, NULL);
        for (int i = 0; i < erf_header->num_entries; i++) {
            cnwn_ERFEntry * entry = cnwn_erf_entry_array_get(array, current + i);
            ret = cnwn_erf_entry_read_key(entry, &version, input_file);
            if (ret < 0) {
                cnwn_set_error("%s (reading key #%d)", cnwn_get_error(), i);
                cnwn_array_set_length(array, current);
                return -1;
            }
        }
        ret = cnwn_file_seek(input_file, erf_header->values_offset);
        if (ret < 0) {
            cnwn_set_error("%s (seeking keys offset %u)", cnwn_get_error(), erf_header->keys_offset);
            cnwn_array_set_length(array, current);
            return -1;
        }
        for (int i = 0; i < erf_header->num_entries; i++) {
            cnwn_ERFEntry * entry = cnwn_erf_entry_array_get(array, current + i);
            ret = cnwn_erf_entry_read_value(entry, input_file);
            if (ret < 0) {
                cnwn_set_error("%s (reading value #%d)", cnwn_get_error(), i);
                cnwn_array_set_length(array, current);
                return -1;
            }
        }
    }
    return erf_header->num_entries;
}

int cnwn_erf_entry_array_write(const cnwn_ERFEntryArray * array, const cnwn_ERFHeader * erf_header, cnwn_File * output_file)
{
    if (array->length < erf_header->num_entries) {
        cnwn_set_error("not enough entries in array (want %d, array has %d)", erf_header->num_entries, array->length);
        return -1;
    }
    if (erf_header->num_entries > 0) {
        int64_t ret = cnwn_file_seek(output_file, erf_header->keys_offset);
        if (ret < 0) {
            cnwn_set_error("%s (seeking keys offset %u)", cnwn_get_error(), erf_header->keys_offset);
            return -1;
        }
        cnwn_Version version = cnwn_version_parse(erf_header->version);
        for (int i = 0; i < erf_header->num_entries; i++) {
            cnwn_ERFEntry * entry = cnwn_erf_entry_array_get(array, i);
            ret = cnwn_erf_entry_write_key(entry, &version, output_file);
            if (ret < 0) {
                cnwn_set_error("%s (writing key #%d)", cnwn_get_error(), i);
                return -1;
            }
        }
        ret = cnwn_file_seek(output_file, erf_header->values_offset);
        if (ret < 0) {
            cnwn_set_error("%s (seeking keys offset %u)", cnwn_get_error(), erf_header->keys_offset);
            return -1;
        }
        for (int i = 0; i < erf_header->num_entries; i++) {
            cnwn_ERFEntry * entry = cnwn_erf_entry_array_get(array, i);
            ret = cnwn_erf_entry_write_value(entry, output_file);
            if (ret < 0) {
                cnwn_set_error("%s (writing value #%d)", cnwn_get_error(), i);
                return -1;
            }
        }
    }
    return erf_header->num_entries;
}

int cnwn_erf_entry_array_append_files(cnwn_ERFEntryArray * array, const cnwn_StringArray * paths)
{
    int num = cnwn_array_get_length(paths);
    int count = 0;
    if (num > 0) {
        int current = cnwn_array_get_length(array);
        cnwn_array_append(array, num, NULL);
        for (int i = 0; i < num; i++) {
            const char * path = cnwn_string_array_get(paths, i);
            char basepart[CNWN_PATH_MAX_SIZE];
            cnwn_path_basepart(basepart, sizeof(basepart), path);
            if (cnwn_strcmp(basepart, "erf-meta") != 0) {
                cnwn_ERFEntry * entry = cnwn_erf_entry_array_get(array, current + count);
                int64_t ret = cnwn_erf_entry_set_from_file(entry, path);
                if (ret < 0) {
                    cnwn_set_error("%s (appending path #%d \"%s\")", cnwn_get_error(), i, path != NULL ? path : "");
                    cnwn_array_set_length(array, current);
                    return -1;
                }
                count++;
            }
        }
        cnwn_array_set_length(array, current + count);
    }
    return count;
}

cnwn_ERFEntry * cnwn_erf_entry_array_get(const cnwn_ERFEntryArray * array, int index)
{
    return cnwn_array_element_ptr(array, index);
}

int cnwn_erf_entry_array_find(const cnwn_ERFEntryArray * array, int index, cnwn_ResourceType resource_type, const char * key)
{
    if (index < array->length)
        index += array->length;
    if (index < 0)
        index = 0;
    for (int i = index; i < array->length; i++) {
        cnwn_ERFEntry * entry = cnwn_erf_entry_array_get(array, i);
        if ((resource_type < 0 || resource_type == entry->type) && (cnwn_strisblank(key) || cnwn_strcmpi(key, entry->key) == 0))
            return i;
    }
    return -1;
}

int cnwn_erf_entry_array_find_by_filename(const cnwn_ERFEntryArray * array, int index, const char * filename)
{
    if (index < array->length)
        index += array->length;
    if (index < 0)
        index = 0;
    for (int i = index; i < array->length; i++) {
        cnwn_ERFEntry * entry = cnwn_erf_entry_array_get(array, i);
        char tmps[CNWN_PATH_MAX_SIZE];
        cnwn_path_basepart(tmps, sizeof(tmps), entry->path);
        if (cnwn_strcmp(filename, tmps) == 0)
            return i;
    }
    return -1;
}

int cnwn_erf_entry_array_find_by_path(const cnwn_ERFEntryArray * array, int index, const char * path)
{
    if (index < array->length)
        index += array->length;
    if (index < 0)
        index = 0;
    for (int i = index; i < array->length; i++) {
        cnwn_ERFEntry * entry = cnwn_erf_entry_array_get(array, i);
        if (cnwn_strcmp(path, entry->path) == 0)
            return i;
    }
    return -1;
}

int cnwn_erf_entry_array_find_by_regexp(const cnwn_ERFEntryArray * array, int index, const cnwn_Regexp * regexp)
{
    if (index < array->length)
        index += array->length;
    if (index < 0)
        index = 0;
    for (int i = index; i < array->length; i++) {
        cnwn_ERFEntry * entry = cnwn_erf_entry_array_get(array, i);
        if (cnwn_regexp_match(regexp, entry->path))
            return i;
    }
    return -1;
}

int cnwn_erf_entry_array_find_by_regexps(const cnwn_ERFEntryArray * array, int index, const cnwn_RegexpArray * regexps)
{
    if (index < array->length)
        index += array->length;
    if (index < 0)
        index = 0;
    for (int i = index; i < array->length; i++) {
        cnwn_ERFEntry * entry = cnwn_erf_entry_array_get(array, i);
        if (cnwn_regexp_array_match_any(regexps, entry->path))
            return i;
    }
    return -1;
}

////////////////////////////////////////////////////////////////
//
//
// ERF helpers.
//
//
////////////////////////////////////////////////////////////////

int cnwn_erf_recalculate(cnwn_ERFHeader * erf_header, const cnwn_LocalizedStringArray * strings, cnwn_ERFEntryArray * entries)
{
    cnwn_Version version = cnwn_version_parse(erf_header->version);
    int keylen = CNWN_ERF_KEYLEN(&version);
    if (keylen == 0) {
        cnwn_set_error("unsupported version: %s", erf_header->version);
        return -1;
    }
    erf_header->num_localized_strings = cnwn_array_get_length(strings);
    erf_header->localized_strings_offset = 160;
    erf_header->localized_strings_size = cnwn_localized_string_get_binary_size(strings);
    erf_header->num_entries = cnwn_array_get_length(entries);
    erf_header->keys_offset = erf_header->localized_strings_offset + erf_header->localized_strings_size;
    erf_header->values_offset = erf_header->keys_offset + erf_header->num_entries * (8 + keylen);
    uint32_t offset = erf_header->values_offset + erf_header->num_entries * 8;
    for (int i = 0; i < erf_header->num_entries; i++) {
        cnwn_ERFEntry * entry = cnwn_erf_entry_array_get(entries, i);
        entry->key_offset = erf_header->keys_offset + i * (8 + keylen);
        entry->value_offset = erf_header->values_offset + i * 8;
        entry->offset = offset;
        offset += entry->size;
    }
    return erf_header->num_entries;
}

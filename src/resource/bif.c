#include "cnwn/bif.h"

int64_t cnwn_bif_header_read(cnwn_BIFHeader * header, cnwn_File * input_file)
{
    memset(header, 0, sizeof(cnwn_BIFHeader));
    int64_t ret = 0;
    int64_t rret;
    header->type = cnwn_resource_type_from_file(input_file, &header->version);
    if (!CNWN_RESOURCE_TYPE_IS_BIF(header->type)) {
        cnwn_set_error("not an BIF type");
        return -1;
    }
    // if (header->version.major != 1 || header->version.minor < 0 || header->version.minor > 1) {
    //     cnwn_set_error("unsupported version %d.%d", header->version.minor, header->version.major);
    //     return -1;
    // }
    ret += 8;
    rret = cnwn_file_readu32le(input_file, &header->variable_rescount);
    if (rret < 0) {
        cnwn_set_error("%s (%s)", cnwn_get_error(), "reading variable rescount");
        return -1;
    }
    ret += rret;
    rret = cnwn_file_readu32le(input_file, &header->fixed_rescount);
    if (rret < 0) {
        cnwn_set_error("%s (%s)", cnwn_get_error(), "reading fixed rescount");
        return -1;
    }
    ret += rret;
    rret = cnwn_file_readu32le(input_file, &header->variable_table_offset);
    if (rret < 0) {
        cnwn_set_error("%s (%s)", cnwn_get_error(), "reading variable table offset");
        return -1;
    }
    ret += rret;
    return ret;
}
        
int cnwn_bif_header_read_entries(const cnwn_BIFHeader * header, cnwn_File * input_file, cnwn_BIFEntry ** ret_entries)
{
    if (ret_entries != NULL)
        *ret_entries = NULL;
    if (header->variable_rescount > 0) {
        int64_t rret = cnwn_file_seek(input_file, header->variable_table_offset);
        if (rret < 0) {
            cnwn_set_error("%s (%s %u)", cnwn_get_error(), "seeking variable table offset", header->variable_table_offset);
            return -1;
        }
        cnwn_BIFEntry * entries = malloc(sizeof(cnwn_BIFEntry) * header->variable_rescount);
        memset(entries, 0, sizeof(cnwn_BIFEntry) * header->variable_rescount);
        //int key_size = (header->version.minor > 0 ? 32 : 16);
        for (int i = 0; i < header->variable_rescount; i++) {
            rret = cnwn_file_readu32le(input_file, &entries[i].id);
            if (rret < 0) {
                cnwn_set_error("%s (%s #%d)", cnwn_get_error(), "reading ID", i);
                free(entries);
                return -1;
            }
            rret = cnwn_file_readu32le(input_file, &entries[i].offset);
            if (rret < 0) {
                cnwn_set_error("%s (%s #%d)", cnwn_get_error(), "reading offset", i);
                free(entries);
                return -1;
            }
            rret = cnwn_file_readu32le(input_file, &entries[i].size);
            if (rret < 0) {
                cnwn_set_error("%s (%s #%d)", cnwn_get_error(), "reading size", i);
                free(entries);
                return -1;
            }
            uint32_t tmp;
            rret = cnwn_file_readu32le(input_file, &tmp);
            if (rret < 0) {
                cnwn_set_error("%s (%s #%d)", cnwn_get_error(), "reading restype", i);
                free(entries);
                return -1;
            }
            entries[i].type = tmp;
        }
        if (ret_entries != NULL)
            *ret_entries = entries;
        else
            free(entries);
        return header->variable_rescount;
    }
    return 0;
}

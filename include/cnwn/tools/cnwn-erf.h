/**
 * @file cnwn-erf.h
 * Part of cnwn: Small C99 library and tools for Neverwinter Nights.
 */
#ifndef CNWN_TOOL_ERF_H
#define CNWN_TOOL_ERF_H

#include "cnwn/resource/erf.h"
#include "cnwn/options.h"

/**
 * Check if the tool command is valid.
 * @param c The tool command enum.
 * @returns True or false.
 */
#define CNWN_ERF_TOOL_COMMAND_VALID(c) ((c) >= CNWN_ERF_TOOL_COMMAND_NONE && (c) < CNWN_MAX_ERF_TOOL_COMMAND)

/**
 * Get the command string.
 * @param c The tool command enum.
 * @returns A pointer to the command string or an empty string.
 */
#define CNWN_ERF_TOOL_COMMAND_STRING(c) ((c) >= CNWN_ERF_TOOL_COMMAND_NONE && (c) < CNWN_MAX_ERF_TOOL_COMMAND ? CNWN_ERF_TOOL_COMMAND_STRINGS[(c)] : CNWN_ERF_TOOL_COMMAND_STRINGS[0])

/**
 * Describes the command for the tool.
 */
enum cnwn_ERFToolCommand_e {

    /**
     * Invalid command.
     */
    CNWN_ERF_TOOL_COMMAND_INVALID = -1,

    /**
     * No command has been given.
     */
    CNWN_ERF_TOOL_COMMAND_NONE = 0,

    /**
     * Info.
     */
    CNWN_ERF_TOOL_COMMAND_INFO,

    /**
     * List.
     */
    CNWN_ERF_TOOL_COMMAND_LIST,

    /**
     * Create.
     */
    CNWN_ERF_TOOL_COMMAND_CREATE,

    /**
     * Extract.
     */
    CNWN_ERF_TOOL_COMMAND_EXTRACT,
    
    /**
     * Max enum.
     */
    CNWN_MAX_ERF_TOOL_COMMAND
};

/**
 * Used as option values.
 */
enum cnwn_ERFToolOption_e {
    
    /**
     * No option.
     */
    CNWN_ERF_TOOL_OPTION_NONE = 0,

    /**
     * Help.
     */
    CNWN_ERF_TOOL_OPTION_HELP,

    /**
     * Version.
     */
    CNWN_ERF_TOOL_OPTION_VERSION,

    /**
     * Info.
     */
    CNWN_ERF_TOOL_OPTION_INFO,
    
    /**
     * Localized strings.
     */
    CNWN_ERF_TOOL_OPTION_LOCSTRINGS,

    /**
     * Summary.
     */
    CNWN_ERF_TOOL_OPTION_SUMMARY,

    /**
     * Size.
     */
    CNWN_ERF_TOOL_OPTION_SIZE,

    /**
     * Offset.
     */
    CNWN_ERF_TOOL_OPTION_OFFSET,

    /**
     * No entries.
     */
    CNWN_ERF_TOOL_OPTION_NOENTRIES,

    /**
     * Set version.
     */
    CNWN_ERF_TOOL_OPTION_SETVERSION,

    /**
     * Set year.
     */
    CNWN_ERF_TOOL_OPTION_SETYEAR,

    /**
     * Set doy.
     */
    CNWN_ERF_TOOL_OPTION_SETDOY,

    /**
     * Quiet.
     */
    CNWN_ERF_TOOL_OPTION_QUIET,

    /**
     * Output.
     */
    CNWN_ERF_TOOL_OPTION_OUTPUT,

    /**
     * No erf-meta.
     */
    CNWN_ERF_TOOL_OPTION_NOMETA,
    
    /**
     * Max enum.
     */
    CNWN_MAX_ERF_TOOL_OPTION
};

/**
 * @see enum cnwn_ERFToolCommand_e
 */
typedef enum cnwn_ERFToolCommand_e cnwn_ERFToolCommand;

/**
 * @see enum cnwn_ERFToolOption_e
 */
typedef enum cnwn_ERFToolOption_e cnwn_ERFToolOption;

/**
 * @see struct cnwn_ERFToolSettings_s
 */
typedef struct cnwn_ERFToolSettings_s cnwn_ERFToolSettings;

/**
 * Settings for the ERF tool.
 */
struct cnwn_ERFToolSettings_s {

    /**
     * The command.
     */
    cnwn_ERFToolCommand command;
    
    /**
     * The ERF file.
     */
    char * erf_file;
    
    /**
     * No output to stdout.
     */
    bool quiet;

    /**
     * Show version.
     */
    bool version;

    /**
     * Show help.
     */
    bool help;

    /**
     * Show info.
     */
    bool info;
    
    /**
     * Show localized strings.
     */
    bool locstrings;

    /**
     * Show summary.
     */
    bool summary;

    /**
     * Show size.
     */
    bool size;

    /**
     * Show offset.
     */
    bool offset;

    /**
     * Don't show entries.
     */
    bool noentries;

    /**
     * Set version.
     */
    const char * setversion;

    /**
     * Set year.
     */
    const char * setyear;

    /**
     * Set doy.
     */
    const char * setdoy;
  
    /**
     * Output.
     */
    const char * output;

    /**
     * Don't do erf-meta.
     */
    bool nometa;
    
    /**
     * Normal arguments.
     */
    cnwn_StringArray arguments;
};

#ifdef __cplusplus
extern "C" {
#endif

/**
 * General options.
 */
extern CNWN_PUBLIC const cnwn_Option CNWN_ERF_TOOL_OPTIONS_GENERAL[];

/**
 * Info options.
 */
extern CNWN_PUBLIC const cnwn_Option CNWN_ERF_TOOL_OPTIONS_INFO[];

/**
 * List options.
 */
extern CNWN_PUBLIC const cnwn_Option CNWN_ERF_TOOL_OPTIONS_LIST[];

/**
 * Create options.
 */
extern CNWN_PUBLIC const cnwn_Option CNWN_ERF_TOOL_OPTIONS_CREATE[];

/**
 * Extract options.
 */
extern CNWN_PUBLIC const cnwn_Option CNWN_ERF_TOOL_OPTIONS_EXTRACT[];

/**
 * Initialize an empty settings.
 * @param settings The settings struct to initialize.
 */
extern CNWN_PUBLIC void cnwn_erf_tool_settings_init(cnwn_ERFToolSettings * settings);

/**
 * Initialize an empty settings from arguments.
 * @param settings The settings struct to initialize.
 * @param[out] ret_error_index Return the index of the invalid argument if this function returns a negative value, otherwise will be -1.
 * @returns The number of parsed arguments or a negative value (@see CNWN_OPTION_ERROR_*).
 * @note The settings do not require deinit if this function returns a negative value.
 */
extern CNWN_PUBLIC int cnwn_erf_tool_settings_init_from_arguments(cnwn_ERFToolSettings * settings, int argc, char * argv[], int * ret_error_index);

/**
 * Deinitialize an empty settings.
 * @param settings The settings deinitialize.
 */
extern CNWN_PUBLIC void cnwn_erf_tool_settings_deinit(cnwn_ERFToolSettings * settings);

/**
 * Execute command.
 * @param settings The settings to use.
 * @returns Zero on success or a negative value on error.
 * @see cnwn_get_error() if this function returns a negative value.
 */
extern CNWN_PUBLIC int cnwn_erf_tool_execute(const cnwn_ERFToolSettings * settings);

/**
 * List the contents of an ERF.
 * @param settings The settings to use.
 * @returns Zero on success or a negative value on error.
 * @see cnwn_get_error() if this function returns a negative value.
 */
extern CNWN_PUBLIC int cnwn_erf_tool_list(const cnwn_ERFToolSettings * settings);

/**
 * Create a new ERF.
 * @param settings The settings to use.
 * @returns Zero on success or a negative value on error.
 * @see cnwn_get_error() if this function returns a negative value.
 */
extern CNWN_PUBLIC int cnwn_erf_tool_create(const cnwn_ERFToolSettings * settings);

/**
 * Extract a new ERF.
 * @param settings The settings to use.
 * @returns Zero on success or a negative value on error.
 * @see cnwn_get_error() if this function returns a negative value.
 */
extern CNWN_PUBLIC int cnwn_erf_tool_extract(const cnwn_ERFToolSettings * settings);

#ifdef __cplusplus
}
#endif

#endif

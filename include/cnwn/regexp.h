/**
 * @file regexp.h
 * Part of cnwn: Small C99 library and tools for Neverwinter Nights.
 */
#ifndef CNWN_REGEXP_H
#define CNWN_REGEXP_H

#include "cnwn/common.h"
#include "cnwn/string.h"
#include "cnwn/containers.h"

/**
 * Definition depends on platform:
 * - Right now all platforms use: struct cnwn_Regexp_s { regex_t regex; };
 */
typedef struct cnwn_Regexp_s cnwn_Regexp;

/**
 * @see cnwn_Array
 */
typedef cnwn_Array cnwn_RegexpArray;

#ifdef __cplusplus
extern "C" {
#endif

////////////////////////////////////////////////////////////////
//
//
// Regexp.
// 
//
////////////////////////////////////////////////////////////////

/**
 * Initialize (and compile) a regular expression.
 * @param regexp The regexp struct to initialize.
 * @param re The regular expression.
 * @returns Zero on success or a negative value on error.
 * @see cnwn_get_error() if this function returns a negative value.
 */
extern CNWN_PUBLIC int cnwn_regexp_init(cnwn_Regexp * regexp, const char * re);

/**
 * Deinitialize a regular expression.
 * @param regexp The regexp to deinitialize.
 */
extern CNWN_PUBLIC void cnwn_regexp_deinit(cnwn_Regexp * regexp);

/**
 * Match a string against the regular expression.
 * @param regexp The regexp to match against.
 * @param s The string to match.
 * @returns True if a match is found, false if not.
 */
extern CNWN_PUBLIC bool cnwn_regexp_match(const cnwn_Regexp * regexp, const char * s);

////////////////////////////////////////////////////////////////
//
//
// Regexp array.
// 
//
////////////////////////////////////////////////////////////////

/**
 * Functions for the regexp array.
 */
extern CNWN_PUBLIC const cnwn_ContainerCallbacks CNWN_REGEXP_ARRAY_FUNCTIONS;

/**
 * Initialize a regexp array.
 * @param regexp_array The regexp array struct to initialize.
 */
extern CNWN_PUBLIC void cnwn_regexp_array_init(cnwn_RegexpArray * regexp_array);

/**
 * Initialize a regexp array from strings.
 * @param regexp_array The regexp array struct to initialize.
 * @param string_array The strings.
 * @returns The number of added regexps or a negative value on error.
 * @see cnwn_get_error() if this function returns a negative value.
 */
extern CNWN_PUBLIC int cnwn_regexp_array_init_from_strings(cnwn_RegexpArray * regexp_array, const cnwn_StringArray * string_array);

/**
 * Append a regexp to the array.
 * @param regexp_array The regexp array to append.
 * @param re The regular expression to append.
 * @returns The new length (i.e number of regular expressions in the array) or a negative error on error.
 * @see cnwn_get_error() if this function returns NULL.
 */
extern CNWN_PUBLIC int cnwn_regexp_array_append(cnwn_RegexpArray * regexp_array, const char * re);

/**
 * Match all (AND).
 * @param regexp_array The regexp array to match against.
 * @param s The string to match.
 * @returns True if all regular expressions in the array matches, false if not.
 * @note If the array is empty the function returns true.
 */
extern CNWN_PUBLIC bool cnwn_regexp_array_match_all(const cnwn_RegexpArray * regexp_array, const char * s);

/**
 * Match any (OR).
 * @param regexp_array The regexp array to match against.
 * @param s The string to match.
 * @returns True if any of the regular expressions in the array matches, false if not.
 * @note If the array is empty the function returns true.
 */
extern CNWN_PUBLIC bool cnwn_regexp_array_match_any(const cnwn_RegexpArray * regexp_array, const char * s);

#ifdef __cplusplus
}
#endif

#endif

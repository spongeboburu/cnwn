/**
 * @file erf.h
 * Part of cnwn: Small C99 library and tools for Neverwinter Nights.
 */
#ifndef CNWN_ERF_H
#define CNWN_ERF_H

#include "cnwn/endian.h"
#include "cnwn/file_system.h"
#include "cnwn/version.h"
#include "cnwn/regexp.h"
#include "cnwn/resource/resource_type.h"
#include "cnwn/resource/localized_strings.h"

/**
 * Check if a char is valid as part of an entry name.
 * @param c The character.
 * @returns True if valid false if not.
 */
#define CNWN_ERF_ENTRY_NAME_CHAR_VALID(c) (((c) >= 'a' && (c) <= 'z') || ((c) >= 'A' && (c) <= 'Z') || ((c) >= '0' && (c) <= '9') || (c) == '_')

/**
 * Check if the ERF version is valid.
 * @param v A pointer to the version.
 * @returns True if valid false if not.
 */
#define CNWN_ERF_VERSION_VALID(v) (((const void *)(v)) != NULL && (v)->major == 1 && ((v)->minor == 0 || (v)->minor == 1))

/**
 * Check if the resource type is ERF compatible.
 * @param t The resource type.
 * @returns True or false.
 */
#define CNWN_RESOURCE_TYPE_IS_ERF(t) ((t) == CNWN_RESOURCE_TYPE_ERF || (t) == CNWN_RESOURCE_TYPE_HAK || (t) == CNWN_RESOURCE_TYPE_MOD || (t) == CNWN_RESOURCE_TYPE_NWM)

/**
 * Get the key size based on a version.
 * @param v A pointer to the version.
 * @returns The key length (32 for 1.1 and 16 for 1.0), will return 0 on invalid version (only 1.0 and 1.1 supported).
 */
#define CNWN_ERF_KEYLEN(v) (CNWN_ERF_VERSION_VALID(v) ? ((v)->minor > 0 ? 32 : 16) : 0)

/**
 * The maximum number of localized strings we will allow (one mil enough?).
 */
#define CNWN_ERF_MAX_LOCALIZED_STRINGS 1048576

/**
 * The maximum number of entries (one mil, but im thinking the toolset wont support that?).
 */
#define CNWN_ERF_MAX_ENTRIES 1048576

/**
 * The size of the header part (in bytes).
 */
#define CNWN_ERF_HEADER_SIZE 160

/**
 * The resource type of the header is invalid for ERF.
 */
#define CNWN_ERF_HEADER_ERROR_TYPE -1

/**
 * The invalid version for ERF.
 */
#define CNWN_ERF_HEADER_ERROR_VERSION -2

/**
 * The maximum number of localized strings exceeded.
 */
#define CNWN_ERF_HEADER_ERROR_MAX_STRINGS -3

/**
 * The strings offset is smaller than the header size.
 */
#define CNWN_ERF_HEADER_ERROR_STRINGS_OFFSET -4

/**
 * The strings size is zero while num strings is not or strings is non-zero while num strings is zero.
 */
#define CNWN_ERF_HEADER_ERROR_STRINGS_SIZE -5

/**
 * The keys offset is smaller than the localized strings offset + localized strings size.
 */
#define CNWN_ERF_HEADER_ERROR_KEYS_OFFSET -6

/**
 * The values offset is smaller than the keys offset + keys size.
 */
#define CNWN_ERF_HEADER_ERROR_VALUES_OFFSET -7

/**
 * The maximum number of entries exceeded.
 */
#define CNWN_ERF_HEADER_ERROR_MAX_ENTRIES -8

/**
 * The key is invalid.
 */
#define CNWN_ERF_ENTRY_ERROR_KEY -101

/**
 * There is a duplicate type/key entry.
 */
#define CNWN_ERF_ENTRY_ERROR_DUPLICATE -102

/**
 * The version is invalid.
 */
#define CNWN_ERF_ENTRY_ERROR_VERSION -103

/**
 * The key offset is either to small, too big or is misaligned.
 */
#define CNWN_ERF_ENTRY_ERROR_KEY_OFFSET -104

/**
 * The value offset is either to small, too big or misaligned.
 */
#define CNWN_ERF_ENTRY_ERROR_VALUE_OFFSET -105

/**
 * @see struct cnwn_ERFHeader_s
 */
typedef struct cnwn_ERFHeader_s cnwn_ERFHeader;

/**
 * @see struct cnwn_ERFEntry_s
 */
typedef struct cnwn_ERFEntry_s cnwn_ERFEntry;

/**
 * @see cnwn_Array
 */
typedef cnwn_Array cnwn_ERFEntryArray;

/**
 * Contains all the data from an ERF header.
 */
struct cnwn_ERFHeader_s {

    /**
     * The resource type.
     */
    char type[5];

    /**
     * The format version.
     */
    char version[5];

    /**
     * The number of localized strings.
     */
    uint32_t num_localized_strings;

    /**
     * The offset of the localized strings.
     */
    uint32_t localized_strings_offset;

    /**
     * The size of the localized strings.
     */
    uint32_t localized_strings_size;

    /**
     * The offset of the keys.
     */
    uint32_t keys_offset;

    /**
     * The offset of the values.
     */
    uint32_t values_offset;
    
    /**
     * The number of entries.
     */
    uint32_t num_entries;

    /**
     * The year.
     */
    uint32_t year;

    /**
     * The day of the year.
     */
    uint32_t day_of_year;

    /**
     * The description strref.
     */
    uint32_t description_strref;

    /**
     * The rest of the header.
     */
    int8_t rest[116];
};

/**
 * An ERF entry.
 */
struct cnwn_ERFEntry_s {

    /**
     * The resource type.
     */
    uint16_t type;
    
    /**
     * The key offset.
     */
    uint32_t key_offset;

    /**
     * The value offset.
     */
    uint32_t value_offset;
    
    /**
     * The offset of the data.
     */
    uint32_t offset;

    /**
     * The size of the data.
     */
    uint32_t size;

    /**
     * The ID.
     */
    uint32_t id;
    
    /**
     * Unused.
     */
    uint16_t unused;

    /**
     * The key (name).
     */
    char * key;
    
    /**
     * The file path.
     */
    char * path;
};

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Functions for the entry array container.
 */
extern CNWN_PUBLIC const cnwn_ContainerCallbacks CNWN_ERF_ENTRY_ARRAY_FUNCTIONS;

////////////////////////////////////////////////////////////////
//
//
// ERF related functions.
//
//
////////////////////////////////////////////////////////////////

/**
 * Check if the entry key is valid.
 * @param key The key to check.
 * @param version The ERF format version, NULL for 1.0.
 * @returns True if the key is valid false if not.
 */
extern CNWN_PUBLIC bool cnwn_erf_entry_key_valid(const char * key, const cnwn_Version * version);

////////////////////////////////////////////////////////////////
//
//
// ERF header.
//
//
////////////////////////////////////////////////////////////////

/**
 * Read header data from an ERF file.
 * @param erf_header The ERF header.
 * @param input_file The file to read from, must be at the seek offset where the header is (usually 0).
 * @returns The number of read bytes or a negative value on error.
 * @see cnwn_get_error() if this function returns a negative value.
 */
extern CNWN_PUBLIC int64_t cnwn_erf_header_read(cnwn_ERFHeader * erf_header, cnwn_File * input_file);

/**
 * Write ERF data to a file.
 * @param erf_header The ERF header.
 * @param output_file The file to write to, must be at the seek offset where the header is (usually 0).
 * @returns The number of written bytes or a negative value on error.
 * @see cnwn_get_error() if this function returns a negative value.
 */
extern CNWN_PUBLIC int64_t cnwn_erf_header_write(const cnwn_ERFHeader * erf_header, cnwn_File * output_file);

/**
 * Set header from erf-info file lines.
 * @param erf_header The ERF header, make sure to use an initialized struct (zeroed if it's brand new).
 * @param strings The erf-info lines to parse.
 * @returns The number of parsed lines or a negative value on error.
 * @see cnwn_get_error() if this function returns a negative value.
 */
extern CNWN_PUBLIC int cnwn_erf_header_parse_info_lines(cnwn_ERFHeader * erf_header, const cnwn_StringArray * strings);

/**
 * Append erf-info lines from header.
 * @param erf_header The ERF header.
 * @param strings The string array to append to.
 * @returns The number of appended lines or a negative value on error.
 * @see cnwn_get_error() if this function returns a negative value.
 */
extern CNWN_PUBLIC int cnwn_erf_header_append_info_lines(const cnwn_ERFHeader * erf_header, cnwn_StringArray * strings);

/**
 * Validate the header by checking that all offsets are in order.
 * @param erf_header The ERF header.
 * @returns Zero on success and a SPECIFIC negative value to indicate an error.
 * @see CNWN_ERF_HEADER_ERROR_* defines for specific return value.
 * @note No error will be set.
 */
extern CNWN_PUBLIC int cnwn_erf_header_validate(const cnwn_ERFHeader * erf_header);

////////////////////////////////////////////////////////////////
//
//
// ERF entry.
//
//
////////////////////////////////////////////////////////////////

/**
 * Init a blank erf entry.
 * @param erf_entry The struct to initialize.
 */
extern CNWN_PUBLIC void cnwn_erf_entry_init(cnwn_ERFEntry * erf_entry);

/**
 * Deinitialize an ERF entry.
 * @param erf_entry The ERF entry.
 */
extern CNWN_PUBLIC void cnwn_erf_entry_deinit(cnwn_ERFEntry * erf_entry);

/**
 * Read ERF entry key (sets key, ID, type, unused, key offset and path) from file.
 * @param erf_entry The ERF entry.
 * @param version Used to the determine the key length, NULL for 1.0.
 * @param input_file The input file, must be at the seek offset where the key is found.
 * @returns The number of read bytes or a negative value on error.
 * @see cnwn_get_error() if this function returns a negative value.
 */
extern CNWN_PUBLIC int64_t cnwn_erf_entry_read_key(cnwn_ERFEntry * erf_entry, const cnwn_Version * version, cnwn_File * input_file);

/**
 * Write ERF entry key to file.
 * @param erf_entry The ERF entry.
 * @param version Used to the determine the key length, NULL for 1.0.
 * @param output_file The output file, must be at the seek offset where the key is found.
 * @returns The number of written bytes or a negative value on error.
 * @see cnwn_get_error() if this function returns a negative value.
 */
extern CNWN_PUBLIC int64_t cnwn_erf_entry_write_key(const cnwn_ERFEntry * erf_entry, const cnwn_Version * version, cnwn_File * output_file);

/**
 * Read ERF entry value (sets value offset, offset and size) from file.
 * @param erf_entry The ERF entry.
 * @param input_file The input file, must be at the seek offset where the key is found.
 * @returns The number of read bytes or a negative value on error.
 * @see cnwn_get_error() if this function returns a negative value.
 */
extern CNWN_PUBLIC int64_t cnwn_erf_entry_read_value(cnwn_ERFEntry * erf_entry, cnwn_File * input_file);

/**
 * Write ERF entry value to file.
 * @param erf_entry The ERF entry.
 * @param output_file The output file, must be at the seek offset where the key is found.
 * @returns The number of written bytes or a negative value on error.
 * @see cnwn_get_error() if this function returns a negative value.
 */
extern CNWN_PUBLIC int64_t cnwn_erf_entry_write_value(const cnwn_ERFEntry * erf_entry, cnwn_File * output_file);

/**
 * Examine the file and set some members (key, type, path and size).
 * @param erf_entry The ERF entry.
 * @param path The path to the file to set the entry from.
 * @returns The size of the resource of the entry or a negative value on error.
 * @see cnwn_get_error() if this function returns a negative value.
 */
extern CNWN_PUBLIC int64_t cnwn_erf_entry_set_from_file(cnwn_ERFEntry * erf_entry, const char * path);

/**
 * Validate the key and offsets.
 * @param erf_entry The ERF entry.
 * @param erf_header The ERF header used to determine version and offsets.
 * @returns Zero on success and a SPECIFIC negative value to indicate an error.
 * @see CNWN_ERF_ENTRY_ERROR_* and CNWN_ERF_HEADER_ERROR_* defines for specific return value.
 * @note No error will be set.
 */
extern CNWN_PUBLIC int cnwn_erf_entry_validate(const cnwn_ERFEntry * erf_entry, const cnwn_ERFHeader * erf_header);

////////////////////////////////////////////////////////////////
//
//
// ERF entry array.
//
//
////////////////////////////////////////////////////////////////

/**
 * Initialize an ERF entry array.
 * @param array The ERF entry array struct to initialize.
 */
extern CNWN_PUBLIC void cnwn_erf_entry_array_init(cnwn_ERFEntryArray * array);

/**
 * Read entries from an ERF file.
 * @param array The ERF entry array to append entries to.
 * @param erf_header The ERF header, used to determine version and seek offsets.
 * @param input_file Read from this file.
 * @returns The number of appended entries.
 * @see cnwn_get_error() if this function returns a negative value.
 * @note If this function returns a negative value the array will discard any read entries.
 */
extern CNWN_PUBLIC int cnwn_erf_entry_array_read(cnwn_ERFEntryArray * array, const cnwn_ERFHeader * erf_header, cnwn_File * input_file);

/**
 * Write entries to an ERF file.
 * @param array The ERF entry array to write entries from.
 * @param erf_header The ERF header, used to determine version and seek offsets.
 * @param output_file Write to this file.
 * @returns The number of written entries.
 * @see cnwn_get_error() if this function returns a negative value.
 * @note The number of written entries will be that described by @p erf_header, an error will be returned if the array length is smaller than the described number of entries in the header.
 */
extern CNWN_PUBLIC int cnwn_erf_entry_array_write(const cnwn_ERFEntryArray * array, const cnwn_ERFHeader * erf_header, cnwn_File * output_file);

/**
 * Append entries from files.
 * @param array The ERF entry array to append to.
 * @param paths The paths of the files.
 * @returns The number of appended entries or a negative value on error.
 * @see cnwn_get_error() if this function returns a negative value.
 */
extern CNWN_PUBLIC int cnwn_erf_entry_array_append_files(cnwn_ERFEntryArray * array, const cnwn_StringArray * paths);

/**
 * Get an entry from the array.
 * @param array The ERF entry array to get from.
 * @param index The index of where to start searching, a negative value will wrap from the end.
 * @returns The ERF entry or NULL if @p index is out of range.
 */
extern CNWN_PUBLIC cnwn_ERFEntry * cnwn_erf_entry_array_get(const cnwn_ERFEntryArray * array, int index);

/**
 * Find an entry.
 * @param array The ERF entry array to search.
 * @param index The index of where to start searching, a negative value will wrap from the end.
 * @param resource_type The resource type of the entry, use a negative value to match any type.
 * @param key The key of the entry, use NULL or an empty string to match any key.
 * @returns The index of the found entry or a negative value on error.
 */
extern CNWN_PUBLIC int cnwn_erf_entry_array_find(const cnwn_ERFEntryArray * array, int index, cnwn_ResourceType resource_type, const char * key);

/**
 * Find an entry by filename (basepart).
 * @param array The ERF entry array to search.
 * @param index The index of where to start searching, a negative value will wrap from the end.
 * @param filename Just the filename part "myentry.ifo" (i.e cnwn_path_basepart()) for instance, if a full path is specified the basepart will be extracted.
 * @returns The index of the found entry or a negative value on error.
 */
extern CNWN_PUBLIC int cnwn_erf_entry_array_find_by_filename(const cnwn_ERFEntryArray * array, int index, const char * filename);

/**
 * Find an entry by path.
 * @param array The ERF entry array to search.
 * @param index The index of where to start searching, a negative value will wrap from the end.
 * @param path Find an entry with matching part.
 * @returns The index of the found entry or a negative value on error.
 */
extern CNWN_PUBLIC int cnwn_erf_entry_array_find_by_path(const cnwn_ERFEntryArray * array, int index, const char * path);

/**
 * Find an entry by path regexp.
 * @param array The ERF entry array to search.
 * @param index The index of where to start searching, a negative value will wrap from the end.
 * @param regexp The regexp to use when searching (will match against the entry path).
 * @returns The index of the found entry or a negative value on error.
 */
extern CNWN_PUBLIC int cnwn_erf_entry_array_find_by_regexp(const cnwn_ERFEntryArray * array, int index, const cnwn_Regexp * regexp);

/**
 * Find an entry by path regexp.
 * @param array The ERF entry array to search.
 * @param index The index of where to start searching, a negative value will wrap from the end.
 * @param regexps The regexp array to use when searching (will match against the entry path).
 * @returns The index of the found entry or a negative value on error.
 */
extern CNWN_PUBLIC int cnwn_erf_entry_array_find_by_regexps(const cnwn_ERFEntryArray * array, int index, const cnwn_RegexpArray * regexps);

////////////////////////////////////////////////////////////////
//
//
// ERF helpers.
//
//
////////////////////////////////////////////////////////////////

/**
 * Update header and entries to reflect the localized strings and entries, see details on what members are updated.
 * @param erf_header The ERF header to update.
 * @param strings The strings.
 * @param entries The entries to update.
 * @returns The number of entries for the erf or a negative value on error (would only happen if @p erf_header version is invalid).
 * @see cnwn_get_error() if this function returns a negative value.
 *
 * Updated members for @p erf_header:
 * - num_localized_strings
 * - localized_strings_offset
 * - localized_strings_size
 * - keys_offset
 * - values_offset
 * - num_entries
 *
 * Updated members for @p entries:
 * - key offset
 * - value offset
 * - offset
 * - id
 */
extern CNWN_PUBLIC int cnwn_erf_recalculate(cnwn_ERFHeader * erf_header, const cnwn_LocalizedStringArray * strings, cnwn_ERFEntryArray * entries);

#ifdef __cplusplus
}
#endif

#endif

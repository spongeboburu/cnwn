/**
 * @file localized_strings.h
 * Part of cnwn: Small C99 library and tools for Neverwinter Nights.
 */
#ifndef CNWN_LOCALIZED_STRINGS_H
#define CNWN_LOCALIZED_STRINGS_H

#include "cnwn/common.h"
#include "cnwn/file_system.h"

/**
 * @see struct cnwn_LocalizedString_s
 */
typedef struct cnwn_LocalizedString_s cnwn_LocalizedString;

/**
 * @see cnwn_Array
 */
typedef cnwn_Array cnwn_LocalizedStringArray;

/**
 * Used for localized strings.
 */
struct cnwn_LocalizedString_s {

    /**
     * Language ID.
     */
    uint32_t language_id;

    /**
     * The string itself.
     */
    char * text;
};

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Initialize a localized string.
 * @param localized_string The struct to initialize.
 * @param language_id The language ID.
 * @param text The string.
 */
extern CNWN_PUBLIC void cnwn_localized_string_init(cnwn_LocalizedString * localized_string, int language_id, const char * text);

/**
 * Initialize the localized string from an ERF file.
 * @param localized_string The struct to initialize.
 * @param input_file The file, must be at the correct offset.
 * @returns The number of read bytes or a negative value on error.
 * @see cnwn_get_error() if this function returns a negative value.
 */
extern CNWN_PUBLIC int64_t cnwn_localized_string_init_read(cnwn_LocalizedString * localized_string, cnwn_File * input_file);

/**
 * Initialize a localized string from an erf-info file line.
 * @param localized_string The struct to initialize.
 * @param s The language and text string as found in erf-info files.
 * @returns The length of the text string or a negative value on error.
 * @see cnwn_get_error() if this function returns a negative value.
 */
extern CNWN_PUBLIC int cnwn_localized_string_init_from_info_line(cnwn_LocalizedString * localized_string, const char * s);

/**
 * Deinitialize a localized string.
 * @param localized_string The localized string to initialize.
 */
extern CNWN_PUBLIC void cnwn_localized_string_deinit(cnwn_LocalizedString * localized_string);

/**
 * Set localized string values from an ERF file.
 * @param localized_string The localized string (already initialized or zeroed).
 * @param input_file The file, must be at the correct offset.
 * @returns The number of read bytes or a negative value on error.
 * @see cnwn_get_error() if this function returns a negative value.
 */
extern CNWN_PUBLIC int64_t cnwn_localized_string_read(cnwn_LocalizedString * localized_string, cnwn_File * input_file);

/**
 * Write the localized string to a file.
 * @param localized_string The localized string.
 * @param output_file The file, must be at the correct offset.
 * @returns The number of written bytes or a negative value on error.
 * @see cnwn_get_error() if this function returns a negative value.
 */
extern CNWN_PUBLIC int64_t cnwn_localized_string_write(const cnwn_LocalizedString * localized_string, cnwn_File * output_file);

/**
 * Set localized string values from an erf-info file line.
 * @param localized_string The localized string (already initialized or zeroed).
 * @param s The language and text string as found in erf-info files.
 * @returns The length of the text string or a negative value on error.
 * @see cnwn_get_error() if this function returns a negative value.
 */
extern CNWN_PUBLIC int cnwn_localized_string_set_from_info_line(cnwn_LocalizedString * localized_string, const char * s);

/**
 * Return a representation of the localized as it would be used in an erf-info file.
 * @param localized_string The localized string to get the erf-info info line from.
 * @param max_size The maximum size incuding zero terminator.
 * @param[out] ret_s Return the string here, pass NULL to just get the required length.
 * @returns The length of the returned string excluding zero terminator.
 */
extern CNWN_PUBLIC int cnwn_localized_string_get_info_line(const cnwn_LocalizedString * localized_string, int max_size, char * ret_s);

/**
 * Return a representation of the localized as it would be used in an erf-info file.
 * @param localized_string The localized string to get the erf-info info line from.
 * @param r The string to realloc, pass NULL to malloc a new string.
 * @returns The erf-info line, must be manually freed.
 */
extern CNWN_PUBLIC char * cnwn_localized_string_get_info_line_realloc(const cnwn_LocalizedString * localized_string, char * r);

////////////////////////////////////////////////////////////////
//
//
// Array
//
//
////////////////////////////////////////////////////////////////

/**
 * Initialize a localized string array.
 * @param array The array of localized strings to init.
 */
extern CNWN_PUBLIC void cnwn_localized_string_array_init(cnwn_LocalizedStringArray * array);

/**
 * Read localized strings from an ERF file and append them to the array.
 * @param array The array of localized strings to read for.
 * @param num The number of strings to read.
 * @param input_file The file, must be at the correct offset.
 * @returns The number of read bytes or a negative value on error.
 * @see cnwn_get_error() if this function returns a negative value.
 * @note If this function returns an error ALL read strings will be discarded and the array returned to it's original length.
 */
extern CNWN_PUBLIC int64_t cnwn_localized_string_array_read(cnwn_LocalizedStringArray * array, int num, cnwn_File * input_file);

/**
 * Write the localized strings to an ERF file.
 * @param array The array of localized strings.
 * @param output_file The file, must be at the correct offset.
 * @returns The number of written bytes or a negative value on error.
 * @see cnwn_get_error() if this function returns a negative value.
 */
extern CNWN_PUBLIC int64_t cnwn_localized_string_array_write(const cnwn_LocalizedStringArray * array, cnwn_File * output_file);

/**
 * Parse info lines and append them to the array.
 * @param array The array of localized strings to read for.
 * @param strings The string array to parse strings from.
 * @returns The number of appended elements or a negative value on error.
 * @see cnwn_get_error() if this function returns a negative value.
 * @note If this function returns an error ALL read strings will be discarded and the array returned to it's original length.
 */
extern CNWN_PUBLIC int cnwn_localized_string_array_parse_info_lines(cnwn_LocalizedStringArray * array, const cnwn_StringArray * strings);

/**
 * Append info lines to a string array from localized strings.
 * @param array The array of localized strings.
 * @param strings The array to append strings to.
 * @returns The number of appended strings.
 */
extern CNWN_PUBLIC int cnwn_localized_string_append_info_lines(const cnwn_LocalizedStringArray * array, cnwn_StringArray * strings);

/**
 * Get a localized string from the array.
 * @param array The array of localized strings.
 * @param index The index of the localized string, negative values will wrap from the end.
 * @returns The localized string or NULL if @p index is out of range.
 */
extern CNWN_PUBLIC const cnwn_LocalizedString * cnwn_localized_string_array_get(const cnwn_LocalizedStringArray * array, int index);

/**
 * Append a localized string to the array.
 * @param array The array of localized strings.
 * @param language_id The lanuage id.
 * @param s The string.
 */
extern CNWN_PUBLIC void cnwn_localized_string_array_append(cnwn_LocalizedStringArray * array, int language_id, const char * s);

/**
 * Insert a localized string into the array.
 * @param array The array of localized strings.
 * @param index Where to insert, negative values will wrap from the end.
 * @param language_id The lanuage id.
 * @param s The string.
 */
extern CNWN_PUBLIC void cnwn_localized_string_array_insert(cnwn_LocalizedStringArray * array, int index, int language_id, const char * s);

/**
 * Set a localized string in the array.
 * @param array The array of localized strings.
 * @param index Where to set, negative values will wrap from the end.
 * @param language_id The lanuage id.
 * @param s The string.
 */
extern CNWN_PUBLIC void cnwn_localized_string_array_set(cnwn_LocalizedStringArray * array, int index, int language_id, const char * s);

/**
 * Append a localized string info line as it would be found in erf-info.
 * @param array The array of localized strings.
 * @param s The info line.
 * @returns The number of appended items or a negative value on error.
 * @see cnwn_get_error() if this function returns a negative value.
 */
extern CNWN_PUBLIC int cnwn_localized_string_array_append_info_line(cnwn_LocalizedStringArray * array, const char * s);

/**
 * Insert a localized string info line as it would be found in erf-info.
 * @param array The array of localized strings.
 * @param index Where to insert, negative values will wrap from the end.
 * @param s The info line.
 * @returns The number of inserted items or a negative value on error.
 * @see cnwn_get_error() if this function returns a negative value.
 */
extern CNWN_PUBLIC int cnwn_localized_string_array_insert_info_line(cnwn_LocalizedStringArray * array, int index, const char * s);

/**
 * Set a localized string info line as it would be found in erf-info.
 * @param array The array of localized strings.
 * @param index Where to set, negative values will wrap from the end.
 * @param s The info line.
 * @returns The number of set items or a negative value on error.
 * @see cnwn_get_error() if this function returns a negative value.
 */
extern CNWN_PUBLIC int cnwn_localized_string_array_set_info_line(cnwn_LocalizedStringArray * array, int index, const char * s);

/**
 * Find a localized string based on it's language ID.
 * @param array The array of localized strings.
 * @param language_id The language id to search for.
 * @returns The index of the localized string or -1 if not found.
 */
extern CNWN_PUBLIC int cnwn_localized_string_array_find(const cnwn_LocalizedStringArray * array, int language_id);

/**
 * Get the size (in bytes) of the string array as it would be in binary form.
 * @param array The array.
 * @returns The size in bytes as the strings would be in binary format.
 */
extern CNWN_PUBLIC uint32_t cnwn_localized_string_get_binary_size(const cnwn_LocalizedStringArray * array);

#ifdef __cplusplus
}
#endif

#endif

/**
 * @file bif.h
 * Part of cnwn: Small C99 library and tools for Neverwinter Nights.
 */
#ifndef CNWN_BIF_H
#define CNWN_BIF_H

#include "cnwn/endian.h"
#include "cnwn/file_system.h"
#include "cnwn/version.h"
#include "cnwn/resource_type.h"

/**
 * @see struct cnwn_BIFHeader_s
 */
typedef struct cnwn_BIFHeader_s cnwn_BIFHeader;

/**
 * @see struct cnwn_BIFEntry_s
 */
typedef struct cnwn_BIFEntry_s cnwn_BIFEntry;

/**
 * Contains all the data from an BIF header.
 */
struct cnwn_BIFHeader_s {

    /**
     * The resource type.
     */
    cnwn_ResourceType type;

    /**
     * The format version.
     */
    cnwn_Version version;

    /**
     * Variable resource count.
     */
    uint32_t variable_rescount;

    /**
     * The offset of the values.
     */
    uint32_t fixed_rescount;
    
    /**
     * The offset for variable table.
     */
    uint32_t variable_table_offset;
};

/**
 * An BIF entry.
 */
struct cnwn_BIFEntry_s {
    
    /**
     * The resource type.
     */
    cnwn_ResourceType type;

    /**
     * The format version.
     */
    cnwn_Version version;
    
    /**
     * The offset of the data.
     */
    uint32_t offset;

    /**
     * The size of the data.
     */
    uint32_t size;

    /**
     * The ID.
     */
    uint32_t id;
    
    /**
     * Unused.
     */
    uint16_t unused;
};

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Set header values by reading from a file.
 * @param header The header struct to initialize.
 * @param input_file The file to read from, must be at the correct offset.
 * @returns The number of read bytes or a negative value on error.
 * @see cnwn_get_error() if this function returns a negative value.
 */
extern CNWN_PUBLIC int64_t cnwn_bif_header_read(cnwn_BIFHeader * header, cnwn_File * input_file);

/**
 * Set header values by reading from a file.
 * @param header The header to read the entries for.
 * @param input_file The file to read from, must be at the correct offset.
 * @param[out] ret_entries Return the pointer to a newly allocated array with entries, must be manually freed.
 * @returns The number of entries returned in the array or a negative value on error.
 * @see cnwn_get_error() if this function returns a negative value.
 * @note If this function returns zero or a negative value then @p ret_entries will be set to NULL.
 */
extern CNWN_PUBLIC int cnwn_bif_header_read_entries(const cnwn_BIFHeader * header, cnwn_File * input_file, cnwn_BIFEntry ** ret_entries);

#ifdef __cplusplus
}
#endif

#endif

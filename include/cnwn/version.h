/**
 * @file version.h
 * Part of cnwn: Small C99 library and tools for Neverwinter Nights.
 */
#ifndef CNWN_VERSION_H
#define CNWN_VERSION_H

#include "cnwn/string.h"
#include "cnwn/file_system.h"

/**
 * @see struct cnwn_Version_e
 */
typedef struct cnwn_Version_s cnwn_Version;

/**
 * Keep track of major/minor version.
 */
struct cnwn_Version_s {

    /**
     * Major.
     */
    int major;

    /**
     * Minor.
     */
    int minor;
};

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Parse a version from a string.
 * @param s The string.
 * @returns The version.
 *
 * Valid formats (where ? is a number):
 * - "?"
 * - "?.?"
 * - "?.??"
 * - "V?"
 * - "V?.?"
 * - "V?.??"
 */
extern CNWN_PUBLIC cnwn_Version cnwn_version_parse(const char * s);

#ifdef __cplusplus
}
#endif

#endif
